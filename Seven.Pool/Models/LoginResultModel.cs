﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seven.Pool.Models
{
    /// <summary>
    /// 登录的结果数据模型
    /// </summary>
    public class LoginResultModel
    {
        /// <summary>
        /// 登录是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 登录失败的提示信息
        /// </summary>
        public string FailMessage { get; set; }

        /// <summary>
        /// 登录成功后获得的用户数据模型
        /// </summary>
        public Member.UserInfoModel Data { get; set; }
    }
}