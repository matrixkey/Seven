﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Seven.Pool.Models
{
    public class LoginModel
    {
        /// <summary>
        /// 登录帐号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get; set; }
    }
}