﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Tools;

namespace Seven.Member.Web.Model
{
    /// <summary>
    /// 登录客户端提交的用户登录身份信息。
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// 初始化类型 <see cref="LoginModel"/> 的新实例。
        /// </summary>
        public LoginModel()
        {
        }

        /// <summary>
        /// 初始化类型 <see cref="LoginModel"/> 的新实例。
        /// </summary>
        /// <param name="account"></param>
        /// <param name="token"></param>
        public LoginModel(string account, string token)
        {
            account = Check.EmptyCheck(account);
            token = Check.EmptyCheck(token);

            this.Account = account;
            this.Token = token;
        }

        /// <summary>
        /// 初始化类型 <see cref="LoginModel"/> 的新实例。
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="account"></param>
        /// <param name="token"></param>
        public LoginModel(string imei, string account, string token)
            : this(account, token)
        {
            imei = Check.EmptyCheck(imei);
            this.IMEI = imei;
        }



        /// <summary>
        /// 表示登录身份信息中的移动设备编码。
        /// </summary>
        public string IMEI
        {
            get;
            set;
        }

        /// <summary>
        /// 表示登录身份信息中的 用户帐号 值。
        /// </summary>
        public string Account
        {
            get;
            set;
        }

        /// <summary>
        /// 表示登录身份信息中的 Token 值。
        /// </summary>
        public string Token
        {
            get;
            set;
        }
    }
}
