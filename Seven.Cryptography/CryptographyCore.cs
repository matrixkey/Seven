﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Cryptography
{
    internal class CryptographyCore
    {
        /// <summary>
        /// 用于对称算法的密钥 注意是8个字符，64位
        /// </summary>
        public readonly static string EncryptKey = "matrixoa";
        /// <summary>
        /// 用于对称算法的初始化向量 注意是8个字符，64位
        /// </summary>
        public readonly static string EncryptIV = "oamatrix";
    }
}
