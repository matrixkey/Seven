﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Web.IntelligentQuery.Model;
using Seven.Web.IntelligentQuery.Extensions;

namespace Seven.Web.IntelligentQuery.Converter
{
    public class QueryConverter
    {
        /// <summary>
        /// 从[like]A=1&B=2中取得[like]A=1，组装成QueryModel对象
        /// </summary>
        /// <param name="parm">string格式的查询条件</param>
        /// <returns></returns>
        public static QueryModel ToQueryModel(string param)
        {
            string temp = param;
            if (string.IsNullOrWhiteSpace(temp))
            {
                return null;
            }
            var model = new QueryModel();
            temp = System.Web.HttpUtility.UrlDecode(temp);
            var dict = temp.Split(MvcHtmlStringExtensions.andStr);
            var keys = dict.Where(c => c.StartsWith(MvcHtmlStringExtensions.methodPrefix.ToString()));//只有[开头的需要处理，[是约定的条件htmlstring格式标记
            foreach (var key in keys)
            {
                var val = key.Split(new char[] { MvcHtmlStringExtensions.equalStr }, 2);//分离字段、值，为防止value中=，只按第一个=分离
                if (string.IsNullOrEmpty(val[1]))
                { continue; }
                model.AddQueryItem(val[0], val[1]);
            }
            return model;
        }

        /// <summary>
        /// 从[like]A=1&B=2中取得[like]A=1，组装成QueryModel对象返回，同时取得B=2组装成Dictionary以输出参数返回
        /// </summary>
        /// <param name="parm">string格式的查询条件</param>
        /// <param name="dic">常规格式的查询条件组装成Dictionary以输出参数返回，Key是参数名，Value是参数值</param>
        /// <returns></returns>
        public static QueryModel ToQueryModel(string param, ref Dictionary<string, string> dic)
        {
            string temp = param;
            if (string.IsNullOrWhiteSpace(temp))
            {
                return null;
            }
            var model = new QueryModel();
            temp = System.Web.HttpUtility.UrlDecode(temp);
            var dict = temp.Split(MvcHtmlStringExtensions.andStr);

            #region QueryModel解析
            var keys = dict.Where(c => c.StartsWith(MvcHtmlStringExtensions.methodPrefix.ToString()));//只有[开头的需要处理，[是约定的条件htmlstring格式标记
            foreach (var key in keys)
            {
                var val = key.Split(new char[] { MvcHtmlStringExtensions.equalStr }, 2);//分离字段、值，为防止value中=，只按第一个=分离
                if (string.IsNullOrEmpty(val[1])) { continue; }
                model.AddQueryItem(val[0], val[1]);
            }
            #endregion

            #region 常规参数解析

            var keysDic = dict.Where(c => !c.StartsWith(MvcHtmlStringExtensions.methodPrefix.ToString()));//只有非[开头的需要处理，[是约定的条件htmlstring格式标记
            foreach (var key in keysDic)
            {
                var val = key.Split(new char[] { MvcHtmlStringExtensions.equalStr }, 2);//分离字段、值，为防止value中=，只按第一个=分离
                if (string.IsNullOrEmpty(val[1])) { continue; }
                dic.Add(val[0], val[1]);
            }

            #endregion

            return model;
        }

        /// <summary>
        /// 从[like]A=1&B=2中取得B=2
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public static Dictionary<string, string> ToDicModel(string parm)
        {
            if (string.IsNullOrWhiteSpace(parm))
            {
                return null;
            }
            var model = new Dictionary<string, string>();
            string tempParm = System.Web.HttpUtility.UrlDecode(parm);
            var dict = tempParm.Split(MvcHtmlStringExtensions.andStr);
            var keys = dict.Where(c => !c.StartsWith(MvcHtmlStringExtensions.methodPrefix.ToString()));//只有非[开头的需要处理，[是约定的条件htmlstring格式标记
            foreach (var key in keys)
            {
                var val = key.Split(new char[] { MvcHtmlStringExtensions.equalStr }, 2);//分离字段、值，为防止value中=，只按第一个=分离
                if (string.IsNullOrEmpty(val[1])) { continue; }
                model.Add(val[0], val[1]);
            }
            return model;
        }
    }
}
