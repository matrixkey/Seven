﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Seven.Web.IntelligentQuery.Model
{
    /// <summary>
    /// 搜索条件集合模型
    /// </summary>
    public class QueryModel
    {
        /// <summary>
        /// 查询条件集合
        /// </summary>
        public List<ConditionItem> Items { get; set; }

        public QueryModel()
        {
            Items = new List<ConditionItem>();
        }
    }

    /// <summary>
    /// 用于存储查询条件的单元
    /// </summary>
    public class ConditionItem
    {
        /// <summary>
        /// 字段名
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// 查询方式，用于标记查询方式，HtmlName中使用[]进行标识
        /// </summary>
        public QueryMethod Method { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// 前缀，用于标记作用域，HTMLName中使用()进行标识
        /// </summary>
        public string Prefix { get; set; }
    }

    /// <summary>
    /// Html表单元素的检索方式
    /// </summary>
    public enum QueryMethod
    {
        /// <summary>
        /// 等于
        /// </summary>
        Equal = 0,

        /// <summary>
        /// 小于
        /// </summary>
        LessThan = 1,

        /// <summary>
        /// 大于
        /// </summary>
        GreaterThan = 2,

        /// <summary>
        /// 小于等于
        /// </summary>
        LessThanOrEqual = 3,

        /// <summary>
        /// 大于等于
        /// </summary>
        GreaterThanOrEqual = 4,

        /// <summary>
        /// 相似
        /// </summary>
        Like = 5,

        /// <summary>
        /// 不等于
        /// </summary>
        NotEqual = 6,

        /// <summary>
        /// 以……起始
        /// </summary>
        StartsWith = 7,

        /// <summary>
        /// 以……截止
        /// </summary>
        EndsWith = 8,





        ///// <summary>
        ///// 在……之内
        ///// </summary>
        //In = 7,

        ///// <summary>
        ///// 处理Like的问题
        ///// </summary>
        //[EditorBrowsable(EditorBrowsableState.Never)]
        //Contains = 12,

        ///// <summary>
        ///// 处理In的问题
        ///// </summary>
        //[EditorBrowsable(EditorBrowsableState.Never)]
        //StdIn = 13,

        ///// <summary>
        ///// 处理Datetime小于+23h59m59s999f的问题
        ///// </summary>
        //[EditorBrowsable(EditorBrowsableState.Never)]
        //DateTimeLessThanOrEqual = 14
    }
}
