﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Seven.EntityBasic;

namespace Seven.Entity
{
    [Entity("SysMenu")]
    public class SysMenu : EntityBase<int>
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Display(Name = "名称")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        [Display(Name = "编号")]
        [Required]
        public string Code { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        [Display(Name = "排序号")]
        public int SortNumber { get; set; }

        /// <summary>
        /// 图标样式
        /// </summary>
        [Display(Name = "图标样式")]
        public string IconCls { get; set; }

        /// <summary>
        /// 父级ID
        /// </summary>
        [Display(Name = "父级ID")]
        public int ParentID { get; set; }

        /// <summary>
        /// 控制器名称
        /// </summary>
        [Display(Name = "控制器名称")]
        public string ControllerName { get; set; }

        /// <summary>
        /// 动作名称
        /// </summary>
        [Display(Name = "动作名称")]
        public string ActionName { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        [Display(Name = "是否显示")]
        public bool IsShow { get; set; }

        /// <summary>
        /// 是否系统菜单
        /// </summary>
        [Display(Name = "是否系统菜单")]
        public bool IsSystem { get; set; }

        /// <summary>
        /// 菜单权限集合
        /// </summary>
        [Display(Name = "菜单功能点集合")]
        public virtual ICollection<SysMenuFunction> MenuFunctions { get; set; }
    }
}
