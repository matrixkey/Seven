﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

using Seven.EntityBasic;
using Seven.Tools.ComponentModel.Paging;

namespace Seven.Core.IRepositories
{
    /// <summary>
    /// 基本仓储接口
    /// </summary>
    /// <typeparam name="TEntity">实体类型</typeparam>
    /// <typeparam name="TKey">TEntity实体的主键类型</typeparam>
    public interface IBaseRepository<TEntity, TKey>
        where TKey : struct
        where TEntity : EntityBase<TKey>
    {
        #region 方法

        /// <summary>
        /// 设置数据对象
        /// </summary>
        /// <param name="context">数据对象</param>
        void SetContext(object context);

        /// <summary>
        /// 主键查询，不对结果集进行跟踪
        /// </summary>
        /// <param name="key">键值</param>
        /// <returns>实体</returns>
        TEntity Find(TKey key);

        /// <summary>
        /// 根据主键集合批量查询实体
        /// </summary>
        /// <param name="keyValues">主键集合</param>
        /// <returns>返回实体数组</returns>
        TEntity[] Select(IEnumerable<object> keyValues);

        /// <summary>
        /// 根据主键集合批量查询实体
        /// </summary>
        /// <param name="keyValues">主键集合</param>
        /// <returns>返回实体数组</returns>
        TEntity[] Select(params object[] keyValues);

        #region 数据操作

        #region 添加

        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="entity">实体</param>
        void EntityAdded(TEntity entity);

        /// <summary>
        /// 批量添加实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        void EntitiesAdded(IEnumerable<TEntity> entities);

        #endregion

        #region 更改

        /// <summary>
        /// 更改实体
        /// </summary>
        /// <param name="entity">实体</param>
        void EntityModified(TEntity entity);

        /// <summary>
        /// 批量更改实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        void EntitiesModified(IEnumerable<TEntity> entities);

        #endregion

        #region 删除

        /// <summary>
        /// 主键删除实体
        /// </summary>
        /// <param name="key">键值</param>
        void EntityDeleted(TKey key);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        void EntityDeleted(TEntity entity);

        /// <summary>
        /// 批量删除实体
        /// </summary>
        /// <param name="entities">实体</param>
        void EntitiesDeleted(IEnumerable<TEntity> entities);

        /// <summary>
        /// 根据条件删除实体
        /// </summary>
        /// <param name="predicate">查询条件</param>
        void EntitiesDeleted(Expression<Func<TEntity, bool>> predicate);

        #endregion

        #endregion

        #endregion
    }
}
