﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.DataModel.RepositoryModels.RoleAbout;
using Seven.DataModel.ViewModels.RoleAbout;

namespace Seven.Core.IRepositories
{
    public partial interface ISysRoleRepository
    {
        #region 获取简单模型数据

        /// <summary>
        /// 根据公司ID获取其拥有的角色集合的简单数据模型
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="includeDisabled">是否包含已禁用的角色</param>
        /// <returns></returns>
        IEnumerable<SimpleModel1> GetSimpleModelByCompanyID(int companyId, bool includeDisabled);

        #endregion

        /// <summary>
        /// 根据角色ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">角色ID</param>
        /// <returns></returns>
        RoleModel GetEditModelByID(int id);

        /// <summary>
        /// 获取角色管理列表数据
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="model">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <returns></returns>
        IEnumerable<RoleGridModel> RoleManageGrid(int? companyId, string model, int page, int rows);

        /// <summary>
        /// 获取指定公司的角色列表数据
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="name">角色名字</param>
        /// <returns></returns>
        IEnumerable<SimpleModel1> RoleGrid(int companyId, string name);

        /// <summary>
        /// 根据公司ID集合，获取他们的下属角色的数量，以Dic形式返回数据，其中Key为ids的元素，Value为其拥有的角色数量
        /// </summary>
        /// <param name="ids">公司ID集合</param>
        /// <returns></returns>
        IDictionary<int, int> GetRolesCountByCompanyIDs(IEnumerable<int> ids);
    }
}
