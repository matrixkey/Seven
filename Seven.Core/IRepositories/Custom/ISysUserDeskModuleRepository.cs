﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.EntityBasic;
using Seven.DataModel.ViewModels.DeskModuleAbout;

namespace Seven.Core.IRepositories
{
    public partial interface ISysUserDeskModuleRepository
    {
        /// <summary>
        /// 根据用户桌面模块ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">用户桌面模块ID</param>
        /// <returns></returns>
        UserDeskModuleModel GetEditModelByID(int id);

        /// <summary>
        /// 根据用户ID获取该用户的桌面模块集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="ignoreDisabled">是否忽略禁用的桌面模块</param>
        /// <returns></returns>
        IEnumerable<SysDeskModule> GetDeskModules(int userId, bool ignoreDisabled = false);

        /// <summary>
        /// 根据用户ID获取该用户有权限的桌面模块的ID集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        IEnumerable<int> GetDeskModuleIDs(int userId);

        /// <summary>
        /// 根据用户ID获取用户有权限的桌面模块数据模型集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="location">位置</param>
        /// <param name="includeNotShow">是否包含“不显示的桌面模块”</param>
        /// <returns></returns>
        IEnumerable<UserDeskModuleModel> GetUserDeskModuleModels(int userId, DeskModuleLocation? location = null, bool includeNotShow = false);

        /// <summary>
        /// 根据用户ID获取用户桌面模块集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="includeDisabled">是否包含禁用的桌面模块</param>
        /// <returns></returns>
        IEnumerable<SysUserDeskModule> GetUserDeskModules(int userId, bool includeDisabled = false);

        /// <summary>
        /// 向所有用户添加指定桌面模块的权限
        /// </summary>
        /// <param name="deskModuleId">桌面模块ID</param>
        /// <returns></returns>
        int InitUserDeskModule(int deskModuleId);

        /// <summary>
        /// 用指定的桌面模块更新所有用户桌面模块中的配置信息，如位置、数据条数、面板高度等
        /// </summary>
        /// <param name="deskModuleId">桌面模块ID</param>
        /// <param name="enforceShow">是否强制显示该桌面模块</param>
        /// <param name="init">是否向新用户初始化该桌面模块</param>
        /// <returns></returns>
        int ResetUserDeskModule(int deskModuleId, bool enforceShow, bool init);

        /// <summary>
        /// 移除指定桌面模块的所有用户桌面模块配置
        /// </summary>
        /// <param name="deskModuleId">桌面模块ID</param>
        /// <returns></returns>
        int RemoveUserDeskModule(int deskModuleId);
    }
}
