﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.RepositoryModels.MenuAbout;
using Seven.DataModel.ViewModels.MenuAbout;

namespace Seven.Core.IRepositories
{
    public partial interface ISysMenuRepository
    {
        #region 获取简单模型数据

        SimpleModel1 GetSimpleModelByID(int id);

        #endregion

        /// <summary>
        /// 根据菜单ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns></returns>
        MenuModel GetEditModelByID(int id);

        /// <summary>
        /// 获取所有菜单列表
        /// </summary>
        /// <returns></returns>
        IEnumerable<SysMenu> GetAllMenus();

        /// <summary>
        /// 根据菜单ID获取菜单名称
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns></returns>
        string GetMenuName(int id);

        /// <summary>
        /// 根据菜单ID获取菜单code
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns></returns>
        string GetMenuCode(int id);

        /// <summary>
        /// 根据菜单ID判定该菜单下是否存在子菜单
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        bool SonMenuExist(int menuId);

        /// <summary>
        /// 根据菜单ID获取同级菜单中最大的排序号
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        int GetMaxSortNumberNearLevel(int menuId);

        /// <summary>
        /// 根据菜单code判定该code是否已经存在
        /// </summary>
        /// <param name="menuCode">菜单code</param>
        /// <returns></returns>
        bool MenuCodeExist(string menuCode);

        /// <summary>
        /// 根据父级ID获取下一级菜单集合
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <returns></returns>
        IEnumerable<SysMenu> GetSons(int parentId);

        /// <summary>
        /// 根据菜单ID集合，获取他们的下一级菜单的数量，以Dic形式返回数据，其中Key为ids的元素，Value为其下一级菜单数量
        /// </summary>
        /// <param name="ids">菜单ID集合</param>
        /// <returns></returns>
        IDictionary<int, int> GetSonsCount(IEnumerable<int> ids);

        /// <summary>
        /// 根据菜单code获取其所有下级菜单
        /// </summary>
        /// <param name="menuCode">菜单code</param>
        /// <returns></returns>
        IEnumerable<SysMenu> GetChildren(string menuCode);
    }
}
