﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.EntityBasic;
using Seven.DataModel.RepositoryModels.DeskModuleAbout;
using Seven.DataModel.ViewModels.DeskModuleAbout;

namespace Seven.Core.IRepositories
{
    public partial interface ISysDeskModuleRepository
    {
        /// <summary>
        /// 根据桌面模块ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">桌面模块ID</param>
        /// <returns></returns>
        DeskModuleModel GetEditModelByID(int id);

        /// <summary>
        /// 获取桌面模块列表数据
        /// </summary>
        /// <param name="location">位置</param>
        /// <returns></returns>
        IEnumerable<DeskModuleGridModel> DeskModuleGrid(DeskModuleLocation? location);

        /// <summary>
        /// 获取所有可用的桌面模块列表数据
        /// </summary>
        /// <returns></returns>
        IEnumerable<SysDeskModule> GetUseableDeskModules();

        /// <summary>
        /// 根据主键ID集合获取桌面模块集合
        /// </summary>
        /// <param name="ids">主键ID集合</param>
        /// <returns></returns>
        IEnumerable<SysDeskModule> GetDeskModules(IEnumerable<int> ids);
    }
}
