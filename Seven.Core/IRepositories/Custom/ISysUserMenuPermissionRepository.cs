﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.RepositoryModels.UserAbout;

namespace Seven.Core.IRepositories
{
    public partial interface ISysUserMenuPermissionRepository
    {
        /// <summary>
        /// 根据用户ID获取该用户的菜单权限集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        IEnumerable<SysUserMenuPermission> GetUserMenuPermissions(int userId);

        /// <summary>
        /// 获取指定用户的限定菜单权限列表数据
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="menuIds">菜单ID集合</param>
        /// <param name="filterUncheck">是否过滤掉“未选中”数据</param>
        /// <returns></returns>
        IEnumerable<UserMenuPermissionGridModel> UserMenuPermissionGrid(int userId, IEnumerable<int> menuIds, bool filterUncheck);
    }
}
