﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.EntityBasic;

namespace Seven.DataModel.ViewModels.DeskModuleAbout
{
    public class UserDeskModuleModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 桌面模块ID
        /// </summary>
        public int DeskModuleID { get; set; }

        /// <summary>
        /// 桌面模块名称
        /// </summary>
        public string DeskModuleName { get; set; }

        /// <summary>
        /// 控制类型
        /// </summary>
        public DeskModuleControlType ControlType { get; set; }

        /// <summary>
        /// 位置
        /// </summary>
        public DeskModuleLocation Location { get; set; }

        /// <summary>
        /// 排序编号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 显示条数
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// 高度
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// 关联的菜单ID
        /// </summary>
        public int MenuID { get; set; }

        /// <summary>
        /// 指向的URL
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 是否接收消息
        /// </summary>
        public bool ReceiveMessage { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public bool IsShow { get; set; }
    }
}
