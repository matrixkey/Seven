﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.DataModel.ViewModels.AccountAbout
{
    public class PasswordModel
    {
        public int UserID { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 原密码
        /// </summary>
        public string OldPassWord { get; set; }

        /// <summary>
        /// 新密码
        /// </summary>
        public string NewPassWord { get; set; }

        /// <summary>
        /// 确认新密码
        /// </summary>
        public string ConfirmPassWord { get; set; }

        /// <summary>
        /// 原密码（加密）
        /// </summary>
        public string OldEnPassWord { get; set; }

        /// <summary>
        /// 新密码（加密）
        /// </summary>
        public string NewEnPassWord { get; set; }
    }
}
