﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.EntityBasic;

namespace Seven.DataModel.ViewModels.StaffAbout
{
    public class StaffModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; }

        /// <summary>
        /// 所在公司ID
        /// </summary>
        public int CompanyID { get; set; }

        /// <summary>
        /// 所在公司
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 所在部门ID
        /// </summary>
        public int DeptID { get; set; }

        /// <summary>
        /// 所在部门
        /// </summary>
        public string DeptName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
