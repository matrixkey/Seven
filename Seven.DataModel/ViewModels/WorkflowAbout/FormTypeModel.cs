﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.DataModel.ViewModels.WorkflowAbout
{
    /// <summary>
    /// 表单类型数据模型
    /// </summary>
    public class FormTypeModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int ID { get; set; }

        /// 类型名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 父级类别
        /// </summary>
        public int ParentID { get; set; }

        /// <summary>
        /// 父级类别
        /// </summary>
        public string ParentName { get; set; }

        /// <summary>
        /// 所属公司ID
        /// </summary>
        public int CompanyID { get; set; }

        /// <summary>
        /// 所属公司名称
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool IsDisabled { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }
    }
}
