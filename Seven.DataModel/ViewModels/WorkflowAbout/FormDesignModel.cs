﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.DataModel.ViewModels.WorkflowAbout
{
    public class FormDesignModel
    {
        /// <summary>
        /// 表单ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 表单名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 表单内容
        /// </summary>
        public string Content { get; set; }
    }
}
