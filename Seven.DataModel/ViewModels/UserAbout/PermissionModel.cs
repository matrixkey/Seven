﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.DataModel.ViewModels.UserAbout
{
    public class PermissionModel
    {
        /// <summary>
        /// 菜单ID
        /// </summary>
        public int MenuID { get; set; }

        /// <summary>
        /// 菜单权限ID
        /// </summary>
        public int MenuPermissionID { get; set; }

        /// <summary>
        /// 打勾状态
        /// </summary>
        public bool Check { get; set; }

        /// <summary>
        /// 选中状态
        /// </summary>
        public bool Indeterminate { get; set; }
    }
}
