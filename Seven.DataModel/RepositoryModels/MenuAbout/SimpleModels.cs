﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.DataModel.RepositoryModels.MenuAbout
{
    public class SimpleModel1
    {
        public string Name { get; set; }

        public string ControllerName { get; set; }

        public string ActionName { get; set; }

        public int FunctionCount { get; set; }
    }
}
