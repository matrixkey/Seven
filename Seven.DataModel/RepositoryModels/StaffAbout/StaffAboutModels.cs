﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.EntityBasic;

namespace Seven.DataModel.RepositoryModels.StaffAbout
{
    public class StaffGridModel
    {
        public int ID { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get;set;}

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 所属公司
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 所属部门
        /// </summary>
        public string DeptName { get; set; }
    }

    public class StaffGridWithoutUserRelationModel
    {
        public int ID { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 所属公司ID
        /// </summary>
        public int CompanyID { get; set; }

        /// <summary>
        /// 所属公司
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// 所属部门
        /// </summary>
        public string DeptName { get; set; }
    }
}
