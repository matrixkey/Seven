﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.DataModel.RepositoryModels.RoleAbout
{
    public class RoleMenuPermissionGridModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleID { get; set; }

        /// <summary>
        /// 菜单权限ID
        /// </summary>
        public int MenuPermissionID { get; set; }

        /// <summary>
        /// 菜单权限名称
        /// </summary>
        public string MenuPermissionName { get; set; }

        /// <summary>
        /// 菜单权限排序号
        /// </summary>
        public int MenuPermissionSortNumber { get; set; }

        /// <summary>
        /// 菜单ID
        /// </summary>
        public int MenuID { get; set; }

        /// <summary>
        /// 打勾状态，用来表示是否有权限
        /// </summary>
        public bool Check { get; set; }
    }
}
