﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Tools.Extension;

namespace Seven.Service
{
    public class SiteCore
    {
        #region 静态只读变量

        /// <summary>
        /// 系统默认的英文名称
        /// </summary>
        public static readonly string DefaultSystemEnglishName = "Seven";
        /// <summary>
        /// 系统自带的超级管理员帐号
        /// </summary>
        public static readonly string DefaultSuperAdminAccountName = "admin";
        /// <summary>
        /// 默认的icon样式名称
        /// </summary>
        public static readonly string DefaultIconClassName = "icon-unknow";
        /// <summary>
        /// 默认的主题名称
        /// </summary>
        public static readonly string DefaultThemeName = "default";
        /// <summary>
        /// 默认的项目根目录名称
        /// </summary>
        public static readonly string DefaultProjectRootName = "Seven";
        /// <summary>
        /// 默认的启动项目的程序集名称
        /// </summary>
        public static readonly string DefaultProjectAssemblyName = "Seven.Site.Web";

        #region 系统cooikes session名称格式

        /// <summary>
        /// 存储“登录名”的cookie名称
        /// </summary>
        public static readonly string LoginNameCookieName = DefaultSystemEnglishName + "_LoginName";
        /// <summary>
        /// 存储“主题皮肤名”的cookies名称前缀
        /// </summary>
        public static readonly string ThemeCookieNamePrefix = DefaultSystemEnglishName + "_Theme_";
        /// <summary>
        /// 存储“组织结构范围”的session名称前缀
        /// </summary>
        public static readonly string OrganRangeCookieNamePrefix = DefaultSystemEnglishName + "_RangeOrgans_";
        /// <summary>
        /// 存储“菜单范围”的session名称前缀
        /// </summary>
        public static readonly string MenuRangeCookieNamePrefix = DefaultSystemEnglishName + "_RangeMenus_";
        /// <summary>
        /// 存储“菜单功能点范围”的session名称前缀
        /// </summary>
        public static readonly string MenuFunctionRangeCookieNamePrefix = DefaultSystemEnglishName + "_RangeMenuFunctions_";
        /// <summary>
        /// 存储“菜单权限范围”的session名称前缀
        /// </summary>
        public static readonly string MenuPermissionRangeCookieNamePrefix = DefaultSystemEnglishName + "_RangeMenuPermissions_";
        /// <summary>
        /// 存储“菜单按钮权限范围”的session名称前缀
        /// </summary>
        public static readonly string MenuButtonPermissionRangeCookieNamePrefix = DefaultSystemEnglishName + "_RangeMenuButtonPermissions_";

        #endregion

        public static class DefaultSystemIcon
        {
            /// <summary>
            /// 默认的“大集团”图标样式名称
            /// </summary>
            public static readonly string DefaultGroupIconClassName = "icon-group";

            /// <summary>
            /// 默认的“小集团”图标样式名称
            /// </summary>
            public static readonly string DefaultSonGroupIconClassName = "icon-son-group";

            /// <summary>
            /// 默认的“公司”图标样式名称
            /// </summary>
            public static readonly string DefaultCompanyIconClassName = "icon-company";

            /// <summary>
            /// 默认的“部门”图标样式名称
            /// </summary>
            public static readonly string DefaultDeptIconClassName = "icon-dept";
        }

        #endregion

        /// <summary>
        /// SignalRService站点的url
        /// </summary>
        public string SignalRServiceUrl;
        /// <summary>
        /// SignalR的集线器名称
        /// </summary>
        public string SignalRHubName;
        /// <summary>
        /// 本系统的用户在SignalR服务中的分组名
        /// </summary>
        public string SiteSignalRGroupName;

        private static bool? _useRemotePool;
        private static int? _sessionTimeout;

        /// <summary>
        /// 获取 Web.config/App.config 中 appSettings 配置集合下的 key: "SevenPool:use" 配置项（注意大小写）的值。
        /// 表示是否使用远程服务池。远程服务池包含了 用户池、SignalR推送。如果 appSettings 中未设定该参数，则取默认值 false。
        /// </summary>
        public static bool UseRemotePool
        {
            get
            {
                if (!_useRemotePool.HasValue)
                {
                    string value = System.Configuration.ConfigurationManager.AppSettings["SevenPool:use"];
                    _useRemotePool = value.ToLower() == "true";
                }
                return _useRemotePool.Value;
            }
        }

        /// <summary>
        /// 获取 Web.config/App.config 中 appSettings 配置集合下的 key: "SEVENAUTH:sessionTimeout" 配置项（注意大小写）的值。
        /// 表示会话状态提供程序终止会话之前各请求之间所允许的时间（以分钟为单位）；如果 appSettings 中未设定该参数，则取默认值 20。
        /// 该值仅在UseRemotePool为false下被使用。当UseRemotePool为true时，超时时间值来自服务池中设置的SessionTimeout。
        /// </summary>
        public static int SessionTimeout
        {
            get
            {
                if (!_sessionTimeout.HasValue)
                {
                    string value = System.Configuration.ConfigurationManager.AppSettings["SEVENAUTH:sessionTimeout"];
                    _sessionTimeout = value.ToInt(20);
                }
                return _sessionTimeout.Value;
            }
        }

        public SiteCore()
        {
            string ss = System.Configuration.ConfigurationManager.AppSettings["SignalRServiceUrl"];
            if (!string.IsNullOrWhiteSpace(ss)) { SignalRServiceUrl = ss.EndsWith("/") ? ss.Substring(0, ss.Length - 1) : ss; }
            else { SignalRServiceUrl = "~"; }

            string sss = System.Configuration.ConfigurationManager.AppSettings["SignalRHubName"];
            if (!string.IsNullOrWhiteSpace(sss)) { SignalRHubName = sss; }
            else { SignalRHubName = "sevenHub"; }

            string ssss = System.Configuration.ConfigurationManager.AppSettings["SiteSignalRGroupName"];
            if (!string.IsNullOrWhiteSpace(ss)) { SiteSignalRGroupName = ssss; }
            else { SiteSignalRGroupName = "Seven"; }
        }

        /// <summary>
        /// 根据组织结构类型获取其对应的预定义的图标样式名称
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetOrganIcon(Seven.EntityBasic.OrganType type)
        {
            string result;
            switch (type)
            {
                case EntityBasic.OrganType.大集团:
                    result = DefaultSystemIcon.DefaultGroupIconClassName;
                    break;
                case EntityBasic.OrganType.子集团:
                    result = DefaultSystemIcon.DefaultSonGroupIconClassName;
                    break;
                case EntityBasic.OrganType.公司:
                    result = SiteCore.DefaultSystemIcon.DefaultCompanyIconClassName;
                    break;
                default:
                    result = SiteCore.DefaultSystemIcon.DefaultDeptIconClassName;
                    break;
            }
            return result;
        }
    }
}
