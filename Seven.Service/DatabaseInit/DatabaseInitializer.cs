﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Service.DatabaseInit
{
    /// <summary>
    /// 数据库初始化操作类
    /// </summary>
    public static class DatabaseInitializer
    {
        /// <summary>
        /// 数据库初始化
        /// </summary>
        public static void Initialize()
        {
            string MigrateDatabase = System.Configuration.ConfigurationManager.AppSettings["MigrateDatabase"];
            bool flag = string.IsNullOrWhiteSpace(MigrateDatabase) ? true : MigrateDatabase.ToLower() == "false" ? false : true;

            DbFactory.DbInitializer.Initialize(flag);
        }
    }
}
