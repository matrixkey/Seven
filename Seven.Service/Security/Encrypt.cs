﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Service.Security
{
    /// <summary>
    /// 加密类【调用Seven.Cryptography加密类】
    /// </summary>
    public class Encrypt
    {
        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="data">要加密的数据</param>
        /// <returns></returns>
        public string Encode(string data)
        {
            return new Seven.Cryptography.Encrypt().Encode(data);
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="data">要解密的数据</param>
        /// <returns></returns>
        public string Decode(string data)
        {
            return new Seven.Cryptography.Encrypt().Decode(data);
        }
    }
}
