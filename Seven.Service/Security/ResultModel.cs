﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Service.Security
{
    public class ResultModel
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 失败的提示信息
        /// </summary>
        public string FailMessage { get; set; }

        /// <summary>
        /// 成功登录获得的身份令牌标识
        /// </summary>
        public Guid Token { get; set; }
    }
}
