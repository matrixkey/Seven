﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using Seven.Entity;
using Seven.DataModel.ViewModels.RoleAbout;
using Seven.Tools.Extension;
using Seven.Tools.Helper;

namespace Seven.Service.Powerful
{
    public partial class SysRoleService
    {
        #region 获取ViewModel

        /// <summary>
        /// 获取新的角色模型
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="companyName">公司名称</param>
        /// <returns></returns>
        public RoleModel GetNewModel(int? companyId, string companyName)
        {
            var model = new RoleModel();
            if (companyId.HasValue) { model.CompanyID = companyId.Value; model.CompanyName = companyName; }

            return model;
        }

        /// <summary>
        /// 根据角色ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">角色ID</param>
        /// <returns></returns>
        public RoleModel GetEditModel(int id)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                return unit.SysRole.GetEditModelByID(id);
            }
        }

        #endregion

        #region 提供json数据

        /// <summary>
        /// 获取角色列表数据
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="model">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <returns></returns>
        public JsonResult RoleManageGrid(int? companyId, string model, int page, int rows)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                var data = unit.SysRole.RoleManageGrid(companyId, model, page, rows);
                return new DataResult(data).SerializeToGridJsonResult(data.Count(), true);
            }
        }

        /// <summary>
        /// 获取指定公司的角色列表数据
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="name">角色名字</param>
        /// <returns></returns>
        public JsonResult RoleGridForSelector(int companyId, string name)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                var data = unit.SysRole.RoleGrid(companyId, name);
                return new DataResult(data).SerializeToJsonResult();
            }
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存角色
        /// </summary>
        /// <param name="model">角色数据模型</param>
        /// <returns>操作结果</returns>
        public JsonResult SaveRole(RoleModel model)
        {
            if (model.ID == 0)
            {
                SysRole role = model.CopyTo<SysRole>();
                return this.Insert(role).SerializeToJsonResult();
            }
            else
            {
                SysRole role = this.GetByKey(model.ID);

                role.Name = model.Name;
                role.Code = model.Code;
                role.SortNumber = model.SortNumber;
                role.Icon = model.Icon;
                role.CompanyID = model.CompanyID;
                if (role.IsDisabled)
                {
                    if (!model.IsDisabled)
                    {
                        role.IsDisabled = model.IsDisabled;
                        role.DisableDate = null;
                        role.DisableUserName = null;
                    }
                }
                else
                {
                    if (model.IsDisabled)
                    {
                        role.IsDisabled = model.IsDisabled;
                        role.DisableDate = DateTime.Now;
                        role.DisableUserName = CurrentUserBaseInfo.UserName;
                    }
                }
                role.Remark = model.Remark;

                return this.Update(role).SerializeToJsonResult();
            }
        }

        /// <summary>
        /// 移除角色
        /// </summary>
        /// <param name="id">角色id</param>
        /// <returns></returns>
        public JsonResult RemoveRole(int id)
        {
            SysRole role;
            using (var unit = DbFactory.UnitOfWork)
            {
                role = unit.SysRole.Find(id); if (role == null) { return new OperationResult(false, true, "角色不存在！").SerializeToJsonResult(); }
                if (unit.SysUser.UseableUserExistByRoleID(role.ID)) { return new OperationResult(false, "该角色下尚存在用户，无法删除！").SerializeToJsonResult(); }

                role.IsDeleted = true;
                role.DeletedDate = DateTime.Now;
                role.Deleteder = CurrentUserBaseInfo.UserName;

                try
                {
                    unit.BeginTransaction();
                    unit.SysRole.EntityModified(role);
                    var msg = new string[] { "删除成功!", "删除失败!" };
                    return new OperationResult(unit.Commit() > 0, msg).SerializeToJsonResult();
                }
                catch (DataAccessException ex)
                {
                    unit.Rollback();
                    throw ExceptionHelper.ThrowServiceException(ex.Message);
                }
            }
        }

        /// <summary>
        /// 禁用/启用角色
        /// </summary>
        /// <param name="id">角色id</param>
        /// <param name="disableState">状态true，则启用；false则禁用</param>
        /// <returns>操作结果</returns>
        public JsonResult EnableOrDisableRole(int id, bool disableState)
        {
            var entity = this.GetByKey(id);
            if (entity == null)
            {
                return new OperationResult(false, "角色不存在！").SerializeToJsonResult();
            }

            entity.IsDisabled = !disableState;
            return this.Update(entity).SerializeToJsonResult();
        }

        #endregion
    }
}
