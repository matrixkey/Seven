﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.MsSql;
using Seven.MsSql.Repositories;

namespace Seven.Service.Powerful
{
    public partial class SysUserBaseConfigService
    {
        /// <summary>
        /// 根据用户名获取该用户的基本配置信息【无配置信息则返回新实例】
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public SysUserBaseConfig GetUserBaseConfig(int userId)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                var BaseConfigRep = unit.SysUserBaseConfig;
                var data = BaseConfigRep.GetUserBaseConfig(userId);
                if (string.IsNullOrWhiteSpace(data.DefaultThemeName)) { data.DefaultThemeName = SiteCore.DefaultThemeName; }
                return data;
            }
        }

        #region CUD操作

        /// <summary>
        /// 保存布局位置
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="region">位置，只能是 north、west、south</param>
        /// <param name="collapse">是否折叠</param>
        public void SaveLayoutState(int userId, string region, bool collapse)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                var BaseConfig = unit.SysUserBaseConfig.GetUserBaseConfig(userId);

                if (BaseConfig.ID == 0)
                {
                    switch (region)
                    {
                        case "north": BaseConfig.NorthCollapse = collapse;
                            break;
                        case "west": BaseConfig.WestCollapse = collapse;
                            break;
                        case "south": BaseConfig.SouthCollapse = collapse;
                            break;
                        default: BaseConfig.WestCollapse = collapse;
                            break;
                    }
                    unit.SysUserBaseConfig.EntityAdded(BaseConfig);
                    unit.Commit();
                }
                else
                {
                    bool need = false;
                    switch (region)
                    {
                        case "north":
                            if (BaseConfig.NorthCollapse != collapse)
                            {
                                BaseConfig.NorthCollapse = collapse; need = true;
                            }
                            break;
                        case "west":
                            if (BaseConfig.WestCollapse != collapse)
                            {
                                BaseConfig.WestCollapse = collapse; need = true;
                            }
                            break;
                        case "south":
                            if (BaseConfig.SouthCollapse != collapse)
                            {
                                BaseConfig.SouthCollapse = collapse; need = true;
                            }
                            break;
                        default:
                            if (BaseConfig.WestCollapse != collapse)
                            {
                                BaseConfig.WestCollapse = collapse; need = true;
                            }
                            break;
                    }
                    if (need)
                    {
                        unit.SysUserBaseConfig.EntityModified(BaseConfig);
                        unit.Commit();
                    }
                }
            }
        }

        /// <summary>
        /// 保存用户默认的主菜单ID
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="menuId">主菜单ID</param>
        public void SaveRootMenuID(int userId, int menuId)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                var BaseConfig = unit.SysUserBaseConfig.GetUserBaseConfig(userId);
                if (BaseConfig == null)
                {
                    BaseConfig.DefaultRootMenuID = menuId;

                    unit.SysUserBaseConfig.EntityAdded(BaseConfig);
                    unit.Commit();
                }
                else
                {
                    bool need = false;
                    if (BaseConfig.DefaultRootMenuID != menuId)
                    {
                        BaseConfig.DefaultRootMenuID = menuId; need = true;
                    }

                    if (need)
                    {
                        unit.SysUserBaseConfig.EntityModified(BaseConfig);
                        unit.Commit();
                    }
                }
            }
        }

        /// <summary>
        /// 保存用户默认的主题
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="themeName">主题名称</param>
        public void SaveThemeName(int userId, string themeName)
        {
            using (var unit = DbFactory.UnitOfWork)
            {
                var BaseConfig = unit.SysUserBaseConfig.GetUserBaseConfig(userId);
                if (BaseConfig == null)
                {
                    BaseConfig.DefaultThemeName = themeName;

                    unit.SysUserBaseConfig.EntityAdded(BaseConfig);
                    unit.Commit();
                }
                else
                {
                    bool need = false;
                    if (BaseConfig.DefaultThemeName != themeName)
                    {
                        BaseConfig.DefaultThemeName = themeName; need = true;
                    }

                    if (need)
                    {
                        unit.SysUserBaseConfig.EntityModified(BaseConfig);
                        unit.Commit();
                    }
                }
            }
        }

        #endregion
    }
}
