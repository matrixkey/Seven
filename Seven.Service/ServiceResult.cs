﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using Seven.Tools.Helper;
using Seven.Web.Mvc;

namespace Seven.Service
{
    /// <summary>
    /// 操作结果
    /// </summary>
    public class OperationResult
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 是否在UI中移除
        /// </summary>
        public bool RemoveOnUI { get; set; }

        /// <summary>
        /// 文本消息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 数据对象
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public OperationResult()
        {

        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="success">是否成功</param>
        /// <param name="message">文本消息</param>
        /// <param name="data">数据对象</param>
        public OperationResult(bool success, string message = null, object data = null)
        {
            this.Success = success;
            this.Message = message;
            this.Data = data;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="success">是否成功</param>
        /// <param name="message">文本消息</param>
        /// <param name="data">数据对象</param>
        public OperationResult(bool success, string[] message, object data = null)
        {
            this.Success = success;
            if (message.Length == 2)
            {
                this.Message = success ? message[0] : message[1];
            }
            this.Data = data;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="success">是否成功</param>
        /// <param name="remove">是否在UI中移除</param>
        /// <param name="message">文本消息</param>
        /// <param name="data">数据对象</param>
        public OperationResult(bool success, bool remove, string message = null, object data = null)
        {
            this.Success = success;
            this.RemoveOnUI = remove;
            this.Message = message;
            this.Data = data;
        }

        /// <summary>
        /// 序列化成JsonResult
        /// </summary>
        /// <returns>JsonResult</returns>
        public JsonResult SerializeToJsonResult()
        {
            return new JsonResult
            {
                Data = new
                {
                    success = this.Success,
                    remove = this.RemoveOnUI,
                    message = this.Message,
                    data = this.Data
                }
            };
        }
    }

    /// <summary>
    /// 数据结果
    /// </summary>
    public class DataResult
    {
        /// <summary>
        /// 数据对象
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public DataResult()
        {

        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="data">数据对象</param>
        public DataResult(object data)
        {
            this.Data = data;
        }

        /// <summary>
        /// 序列化成JsonResult
        /// </summary>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text，默认否</param>
        /// <param name="dateFormatType">序列化时对日期数据的格式化类型，默认不进行转换</param>
        /// <returns>JsonResult</returns>
        public JsonResult SerializeToJsonResult(bool enumTransfer = false, DateFormatType dateFormatType = DateFormatType.None)
        {
            if (!enumTransfer && dateFormatType == DateFormatType.None)
            {
                return new JsonResult
                {
                    Data = this.Data
                };
            }
            else
            {
                return new Seven.Web.Mvc.JsonSerializeResult
                {
                    Data = this.Data,
                    EnumTransfer = enumTransfer,
                    DateFormatType = dateFormatType
                };
            }
        }

        /// <summary>
        /// 序列化成GridJsonResult
        /// </summary>
        /// <param name="total">数据总数</param>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text，默认否</param>
        /// <param name="dateFormatType">序列化时对日期数据的格式化类型，默认不进行转换</param>
        /// <returns></returns>
        public JsonResult SerializeToGridJsonResult(int total = 0, bool enumTransfer = false, DateFormatType dateFormatType = DateFormatType.None)
        {
            return new Seven.Web.Mvc.JsonSerializeResult
            {
                Data = new { total = total, rows = this.Data },
                EnumTransfer = enumTransfer,
                DateFormatType = dateFormatType
            };
        }
    }
}
