﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using Seven.Service.Security;

namespace Seven.Site
{
    [Seven.Site.Mvc.Filters.Auth]
    [Seven.Site.Mvc.Filters.UpdateFormsAuthentication]
    public abstract class BaseController : Controller
    {
        private UserInfoModel _CurrentUserBaseInfo;

        private IEnumerable<Seven.Entity.HrOrganization> _CurrentUserOrganRange;

        private IEnumerable<Seven.Entity.SysMenu> _CurrentUserMenuRange;

        private IEnumerable<Seven.Entity.SysMenuFunction> _CurrentUserMenuFunctionRange;

        /// <summary>
        /// 当前登录用户信息集合（不包含范围信息）
        /// </summary>
        public UserInfoModel CurrentUserBaseInfo
        {
            get
            {
                if (_CurrentUserBaseInfo == null) { _CurrentUserBaseInfo = new Account().GetCurrentUserBaseInfo(); } return _CurrentUserBaseInfo;
            }
        }

        /// <summary>
        /// 当前用户的可管理的组织结构范围
        /// </summary>
        public IEnumerable<Seven.Entity.HrOrganization> CurrentUserOrganRange
        {
            get { if (_CurrentUserOrganRange == null) { _CurrentUserOrganRange = new Account().GetCurrentUserOrganRange(User.Identity.Name); } return _CurrentUserOrganRange; }
        }

        /// <summary>
        /// 当前用户的有权使用的菜单功能点范围，附带菜单导航属性的数据
        /// </summary>
        protected IEnumerable<Seven.Entity.SysMenuFunction> CurrentUserMenuFunctionRange
        {
            get
            {
                if (_CurrentUserMenuFunctionRange == null)
                { _CurrentUserMenuFunctionRange = new Account().GetCurrentUserMenuFunctionRange(User.Identity.Name); }
                return _CurrentUserMenuFunctionRange;
            }
        }

        /// <summary>
        /// 当前用户的有权使用的菜单范围
        /// </summary>
        protected IEnumerable<Seven.Entity.SysMenu> CurrentUserMenuRange
        {
            get { if (_CurrentUserMenuRange == null) { _CurrentUserMenuRange = new Account().GetCurrentUserMenuRange(User.Identity.Name); } return _CurrentUserMenuRange; }
        }
    }
}
