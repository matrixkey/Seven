﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Seven.Site.Mvc.Attributes
{
    /// <summary>
    /// 允许匿名访问标记，也可使用自带的AllowAnonymous特性，该特性会在auth时触发OnAuthorization而不会进入AuthorizeCore
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class AnonymousAttribute : Attribute
    {
    }

    /// <summary>
    /// 只要登录就有权限使用的标记
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class LoginAttribute : Attribute
    {
    }

    /// <summary>
    /// 表示当前Action请求为一个具体的功能页面
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ViewPageAttribute : Attribute
    {
    }

    /// <summary>
    /// 自定义相应编码，用来多层Auth过滤时判定使用
    /// </summary>
    public enum CustomStatusCode
    {
        无权访问 = 318,
        登录超时 = 319,
        未登录 = 320
    }

    /// <summary>
    /// action类型，捕获异常后反馈时使用，用来决定action的result是jsonresult还是viewresult
    /// </summary>
    public enum ActionType
    { 
        视图= 1,
        数据 = 2,
        操作 = 4,
        忽略异常 = 8
    }
}
