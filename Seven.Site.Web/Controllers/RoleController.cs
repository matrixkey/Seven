﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using Seven.DataModel.ViewModels.RoleAbout;
using Seven.Service;
using Seven.Site.Mvc;
using Seven.Site.Mvc.Attributes;
using Seven.Site.Mvc.Filters;

namespace Seven.Site.Web.Controllers
{
    public class RoleController : BaseController
    {
        #region 页面

        /// <summary>
        /// 角色及其权限管理页面
        /// </summary>
        /// <returns></returns>
        [Description("角色及其权限管理页面")]
        [GetButtonPermissions("role")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public ViewResult ManageIndex()
        {
            ViewBag.CurrentRoleID = CurrentUserBaseInfo.RoleID;

            return View();
        }

        #region 角色管理页面

        /// <summary>
        /// 角色新建页面
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="companyName">公司名称</param>
        /// <returns></returns>
        [Description("角色新建页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult RoleCreateForm(int? companyId, string companyName)
        {
            var model = ServiceFactory.SysRole.GetNewModel(companyId, companyName);
            
            return PartialView(model);
        }

        /// <summary>
        /// 角色编辑页面
        /// </summary>
        /// <param name="id">角色id</param>
        /// <returns></returns>
        [Description("角色编辑页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult RoleEditForm(int id)
        {
            var model = ServiceFactory.SysRole.GetEditModel(id);

            return PartialView("RoleCreateForm", model);
        }

        #endregion

        #region 角色权限设置tab页面

        /// <summary>
        /// 角色权限设置tab页面，尝试部分页
        /// </summary>
        /// <returns></returns>
        [Description("角色权限设置tab页面")]
        [GetButtonPermissions("permission", "ManageIndex")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult PermissionManage()
        {
            return PartialView();
        }



        #endregion

        #endregion

        #region 数据

        /// <summary>
        /// 提供角色管理列表信息json数据
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="model">自定义搜索条件模型</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <returns></returns>
        [Description("提供角色信息json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult RoleManageJson(int? companyId, string model, int? page, int? rows)
        {
            int Page = page ?? 1;
            int Rows = rows ?? 10;

            return ServiceFactory.SysRole.RoleManageGrid(companyId, model, Page, Rows);
        }

        /// <summary>
        /// 提供角色菜单权限json数据
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <param name="rangeFilter">是否根据管理范围过滤，默认过滤</param>
        /// <returns></returns>
        [Description("提供角色菜单权限json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult RoleMenuPermissionJson(int roleId, bool rangeFilter = true)
        {
            return ServiceFactory.SysRoleMenuPermission.RoleMenuPermissionGrid(roleId, rangeFilter);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 角色保存请求
        /// </summary>
        /// <param name="model">角色数据模型</param>
        /// <returns></returns>
        [Description("角色保存请求")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveRole(RoleModel model)
        {
            return ServiceFactory.SysRole.SaveRole(model);
        }

        /// <summary>
        /// 移除角色
        /// </summary>
        /// <param name="id">角色ID</param>
        /// <returns></returns>
        [Description("移除角色")]
        [CatchException(ActionType.操作)]
        public JsonResult RemoveRole(int id)
        {
            return ServiceFactory.SysRole.RemoveRole(id);
        }

        /// <summary>
        /// 禁用/启用角色
        /// </summary>
        /// <param name="id">角色id</param>
        /// <param name="disableState">状态true，则启用；false则禁用</param>
        /// <returns></returns>
        [Description("禁用/启用角色")]
        [CatchException(ActionType.操作)]
        public JsonResult EnableOrDisableRole(int id, bool disableState)
        {
            return ServiceFactory.SysRole.EnableOrDisableRole(id, disableState);
        }

        /// <summary>
        /// 保存角色权限
        /// </summary>
        /// <param name="permission">角色权限</param>
        /// <param name="roleId">角色ID</param>
        /// <param name="otherRoleIds">应用到其他角色的id</param>
        /// <returns></returns>
        [Description("保存角色权限请求")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveRolePermission([ModelBinder(typeof(JsonBinder<PermissionModel>))]IEnumerable<PermissionModel> permission, int roleId, string otherRoleIds)
        {
            return ServiceFactory.SysRoleMenuPermission.SaveRolePermission(permission, roleId, otherRoleIds);
        }

        #endregion

        #region 其他

        #endregion
    }
}