﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using Seven.DataModel.ViewModels.WorkflowAbout;
using Seven.Service;
using Seven.Site.Mvc.Attributes;
using Seven.Site.Mvc.Filters;

namespace Seven.Site.Web.Controllers
{
    public class WorkflowFormController : BaseController
    {
        #region 页面

        /// <summary>
        /// 表单管理页面
        /// </summary>
        /// <returns></returns>
        [Description("表单管理页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        [GetButtonPermissions("type,form")]
        public ViewResult ManageIndex()
        {
            return View();
        }

        #region 表单类型管理页面

        /// <summary>
        /// 表单类型新建页面
        /// </summary>
        /// <param name="parentId">父级类型ID</param>
        /// <returns></returns>
        [Description("表单类型新建页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult FormTypeCreateForm(int parentId)
        {
            FormTypeModel model = ServiceFactory.WfFormType.GetNewModel(parentId);

            return PartialView(model);
        }

        /// <summary>
        /// 表单类型编辑页面
        /// </summary>
        /// <param name="id">表单类型ID</param>
        /// <returns></returns>
        [Description("表单类型编辑页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult FormTypeEditForm(int id)
        {
            FormTypeModel model = ServiceFactory.WfFormType.GetEditModel(id);

            return PartialView("FormTypeCreateForm", model);
        }

        #endregion

        #region 表单管理页面

        /// <summary>
        /// 表单新建页面
        /// </summary>
        /// <param name="typeId">表单类型ID</param>
        /// <returns></returns>
        [Description("表单新建页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult FormCreateForm(int typeId)
        {
            FormModel model = ServiceFactory.WfForm.GetNewModel(typeId);

            return PartialView(model);
        }

        /// <summary>
        /// 表单编辑页面
        /// </summary>
        /// <param name="id">表单ID</param>
        /// <returns></returns>
        [Description("表单编辑页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult FormEditForm(int id)
        {
            FormModel model = ServiceFactory.WfForm.GetEditModel(id);

            return PartialView("FormCreateForm", model);
        }

        /// <summary>
        /// 表单设计页面
        /// </summary>
        /// <param name="id">表单ID</param>
        /// <returns></returns>
        [Description("表单设计页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        [AnonymousAttribute]
        public ViewResult FormDesignForm(int id)
        {
            FormDesignModel model = ServiceFactory.WfForm.GetDesignModel(id);

            return View(model);
        }

        #endregion

        #endregion

        #region 数据

        /// <summary>
        /// 异步获取表单类型json数据
        /// </summary>
        /// <param name="id">父级ID</param>
        /// <param name="filter">是否根据当前用户的管理范围过滤</param>
        /// <returns></returns>
        [Description("提供表单类型json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult FormTypeJson(int id = 0, bool filter = false)
        {
            return ServiceFactory.WfFormType.FormTypeJson(id, filter);
        }

        /// <summary>
        /// 获取表单列表json数据
        /// </summary>
        /// <param name="typeId">表单类型ID</param>
        /// <param name="model">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <returns></returns>
        [Description("获取表单列表json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult FormJson(int typeId, string model, int? page, int? rows)
        {
            int Page = page ?? 1;
            int Rows = rows ?? 10;

            return ServiceFactory.WfForm.FormJson(typeId, model, Page, Rows);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存表单类型
        /// </summary>
        /// <param name="model">表单类型数据模型</param>
        /// <returns></returns>
        [Description("保存表单类型")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveFormType(FormTypeModel model)
        {
            return ServiceFactory.WfFormType.SaveFormType(model);
        }

        /// <summary>
        /// 移除表单类型
        /// </summary>
        /// <param name="id">表单类型ID</param>
        /// <returns></returns>
        [Description("移除表单类型")]
        [CatchException(ActionType.操作)]
        public JsonResult RemoveFormType(int id)
        {
            return ServiceFactory.WfFormType.RemoveFormType(id);
        }

        /// <summary>
        /// 保存表单
        /// </summary>
        /// <param name="model">表单数据模型</param>
        /// <returns></returns>
        [Description("保存表单")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveForm(FormModel model)
        {
            return ServiceFactory.WfForm.SaveForm(model);
        }

        /// <summary>
        /// 移除表单
        /// </summary>
        /// <param name="id">表单ID</param>
        /// <returns></returns>
        [Description("移除表单")]
        [CatchException(ActionType.操作)]
        public JsonResult RemoveForm(int id)
        {
            return ServiceFactory.WfForm.RemoveForm(id);
        }

        /// <summary>
        /// 保存表单设计
        /// </summary>
        /// <param name="id">表单ID</param>
        /// <param name="content">表单内容</param>
        /// <returns></returns>
        [Description("保存表单设计")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveFormDesign(int id, string content)
        {
            return Json(new { success = true });
        }

        #endregion

        #region 其他

        #endregion
    }
}