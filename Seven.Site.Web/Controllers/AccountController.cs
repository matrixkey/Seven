﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using Seven.Service;
using Seven.Service.Security;
using Seven.Site.Models.AccountModel;
using Seven.Site.Mvc.Attributes;

namespace Seven.Site.Web.Controllers
{
    public class AccountController : BaseController
    {
        #region 页面

        /// <summary>
        /// 登录页面
        /// </summary>
        /// <returns></returns>
        [Description("登录页面")]
        [HttpGet]
        [AllowAnonymous]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public ViewResult Login()
        {
            var Model = new LoginModel();
            Model.RememberLoginName = true;
            HttpCookie UserNameCookie = Request.Cookies[SiteCore.LoginNameCookieName];
            if (UserNameCookie != null)
            {
                Model.UserName = new Service.Security.Encrypt().Decode(UserNameCookie.Value);
            }

            return View(Model);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 登录操作
        /// </summary>
        /// <param name="model">登录数据模型</param>
        /// <returns></returns>
        [Description("登录操作")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        [AllowAnonymous]
        [CatchException(ActionType.视图)]
        public ActionResult Login(LoginModel model)
        {
            OperationResult Result = new Account().Login(model);
            if (Result.Success)
            {
                return RedirectToAction("Index", "Home", new { });
            }
            else
            {
                model.Message = Result.Message;
            }

            return View(model);
        }

        /// <summary>
        /// 注销操作
        /// </summary>
        /// <returns></returns>
        [Description("注销操作")]
        [Login]
        [CatchException(ActionType.操作)]
        public JsonResult Logout()
        {
            OperationResult result = new Account().Logout(User.Identity.Name);

            return result.SerializeToJsonResult();
        }

        /// <summary>
        /// 测试异常
        /// </summary>
        /// <returns></returns>
        [Description("测试异常")]
        [Login]
        [CatchException(ActionType.操作)]
        public JsonResult TestForError()
        {
            throw new Exception("本项测试旨在检测“手动抛出异常后由异常捕获器捕获并根据Action类型自动判定返回视图还是json数据”，再点击“修改密码”试试。");
            return Json(new { success = true });
        }

        #endregion
    }
}