﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using Seven.DataModel.ViewModels.DeskModuleAbout;
using Seven.Service;
using Seven.Site.Mvc.Attributes;
using Seven.Site.Mvc.Filters;

namespace Seven.Site.Web.Controllers
{
    public class DeskModuleController : BaseController
    {
        #region 页面

        #region 系统设置-桌面模块设置页面

        /// <summary>
        /// 桌面模块管理页面
        /// </summary>
        /// <returns></returns>
        [Description("桌面模块管理页面")]
        [GetButtonPermissions("deskModule")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public ViewResult ManageIndex()
        {
            return View();
        }

        /// <summary>
        /// 新建桌面模块页面
        /// </summary>
        /// <returns></returns>
        [Description("新建桌面模块页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult DeskModuleCreateForm()
        {
            var model = ServiceFactory.SysDeskModule.GetNewModel();

            return PartialView(model);
        }

        /// <summary>
        /// 编辑桌面模块页面
        /// </summary>
        /// <param name="id">桌面模块ID</param>
        /// <returns></returns>
        [Description("编辑桌面模块页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult DeskModuleEditForm(int id)
        {
            var model = ServiceFactory.SysDeskModule.GetEditModel(id);

            return PartialView("DeskModuleCreateForm", model);
        }

        #endregion

        #region 系统设置-桌面模块权限设置

        /// <summary>
        /// 桌面模块权限设置页面
        /// </summary>
        /// <returns></returns>
        [Description("桌面模块权限设置页面")]
        [GetButtonPermissions("deskModulePermission", "ManageIndex")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult PermissionManage()
        {
            return PartialView();
        }

        #endregion

        #region 控制面板-用户桌面模块设置

        /// <summary>
        /// 用户桌面模块设置
        /// </summary>
        /// <returns></returns>
        [Description("用户桌面模块设置")]
        [GetButtonPermissions("userDeskModule")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public ViewResult UserManageIndex()
        {
            ViewBag.CurrentUserID = CurrentUserBaseInfo.UserID;

            return View();
        }

        /// <summary>
        /// 用户桌面模块编辑页面
        /// </summary>
        /// <param name="id">用户桌面模块ID</param>
        /// <returns></returns>
        [Description("用户桌面模块编辑页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult UserDeskModuleEditForm(int id)
        {
            var model = ServiceFactory.SysUserDeskModule.GetEditModel(id);

            return PartialView(model);
        }

        #endregion

        #endregion

        #region 数据

        /// <summary>
        /// 提供桌面模块信息json数据
        /// </summary>
        /// <param name="location">位置，值可以是 left 或者 right</param>
        /// <returns></returns>
        [Description("提供桌面模块信息json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult DeskModuleJson(string location)
        {
            return ServiceFactory.SysDeskModule.DeskModuleGrid(location);
        }

        /// <summary>
        /// 一次性提供桌面模块信息json数据
        /// </summary>
        /// <returns></returns>
        [Description("一次性提供桌面模块信息json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult DeskModuleJsonForOnce()
        {
            return ServiceFactory.SysDeskModule.DeskModuleJsonForOnce();
        }

        /// <summary>
        /// 提供控制类型json数据
        /// </summary>
        /// <returns></returns>
        [Description("提供控制类型json数据")]
        [Login]
        [CatchException(ActionType.数据)]
        public JsonResult ControlTypeJson()
        {
            return ServiceFactory.SysDeskModule.ControlTypeJson();
        }

        /// <summary>
        /// 提供位置json数据
        /// </summary>
        /// <returns></returns>
        [Description("提供位置json数据")]
        [Login]
        [CatchException(ActionType.数据)]
        public JsonResult LocationJson()
        {
            return ServiceFactory.SysDeskModule.LocationJson();
        }

        /// <summary>
        /// 提供用户桌面模块权限信息json数据
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        [Description("提供用户桌面模块权限信息json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult UserDeskModulePermissionJson(int userId)
        {
            return ServiceFactory.SysUserDeskModule.UserDeskModulePermissionJson(userId);
        }

        /// <summary>
        /// 提供用户桌面模块信息json数据
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="location">位置，值可以是 left 或者 right</param>
        /// <returns></returns>
        [Description("提供用户桌面模块信息json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult UserDeskModuleJson(int userId, string location)
        {
            return ServiceFactory.SysUserDeskModule.UserDeskModuleGrid(userId, location);
        }

        /// <summary>
        /// 一次性提供用户桌面模块信息json数据
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        [Description("一次性提供用户桌面模块信息json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult UserDeskModuleJsonForOnce(int userId)
        {
            return ServiceFactory.SysUserDeskModule.UserDeskModuleJsonForOnce(userId);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存桌面模块请求
        /// </summary>
        /// <param name="model">桌面模块数据模型</param>
        /// <returns></returns>
        [Description("保存桌面模块请求")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveDeskModule(DeskModuleModel model)
        {
            return ServiceFactory.SysDeskModule.SaveDeskModule(model);
        }

        /// <summary>
        /// 删除桌面模块请求
        /// </summary>
        /// <param name="id">桌面模块ID</param>
        /// <returns></returns>
        [Description("删除桌面模块请求")]
        [CatchException(ActionType.操作)]
        public JsonResult RemoveDeskModule(int id)
        {
            return ServiceFactory.SysDeskModule.RemoveDeskModule(id);
        }

        /// <summary>
        /// 保存用户桌面权限请求
        /// </summary>
        /// <param name="deskModuleIds">桌面模块IDs，以“,”相连</param>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        [Description("保存用户桌面权限请求")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveUserDeskModulePermission(string deskModuleIds, int userId)
        {
            return ServiceFactory.SysUserDeskModule.SaveUserDeskModulePermission(deskModuleIds, userId);
        }

        /// <summary>
        /// 保存用户桌面模块设置请求
        /// </summary>
        /// <param name="model">用户桌面模块设置数据模型</param>
        /// <returns></returns>
        [Description("保存用户桌面模块设置请求")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveUserDeskModule(UserDeskModuleModel model)
        {
            return ServiceFactory.SysUserDeskModule.SaveUserDeskModule(model);
        }

        #endregion

        #region 其他

        #endregion
    }
}