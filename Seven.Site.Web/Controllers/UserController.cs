﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using Seven.DataModel.ViewModels.AccountAbout;
using Seven.DataModel.ViewModels.UserAbout;
using Seven.Service;
using Seven.Site.Mvc;
using Seven.Site.Mvc.Attributes;
using Seven.Site.Mvc.Filters;

namespace Seven.Site.Web.Controllers
{
    public class UserController : BaseController
    {
        #region 页面

        /// <summary>
        /// 用户及其权限管理页面
        /// </summary>
        /// <returns></returns>
        [Description("用户及其权限管理页面")]
        [GetButtonPermissions("user")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public ViewResult ManageIndex()
        {
            ViewBag.CurrentUserID = CurrentUserBaseInfo.UserID;

            return View();
        }

        #region 用户管理页面

        /// <summary>
        /// 用户新建页面
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <returns></returns>
        [Description("用户新建页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult UserCreateForm(int companyId, int deptId)
        {
            var model = ServiceFactory.SysUser.GetNewModel();

            model.CompanyID = companyId;
            model.DeptID = deptId;

            return PartialView(model);
        }

        /// <summary>
        /// 用户编辑页面
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns></returns>
        [Description("用户编辑页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult UserEditForm(int id)
        {
            var model = ServiceFactory.SysUser.GetEditModel(id);

            return PartialView("UserCreateForm", model);
        }

        /// <summary>
        /// 重置密码页面
        /// </summary>
        /// <param name="id">用户id</param>
        /// <param name="userName">用户名</param>
        /// <param name="name">姓名</param>
        /// <returns></returns>
        [Description("重置密码页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult ResetPassWordForm(int id, string userName, string name)
        {
            var model = new PasswordModel();
            model.UserID = id;
            model.UserName = userName;
            model.RealName = Server.UrlDecode(name);

            return PartialView(model);
        }

        #endregion

        #region 用户权限设置tab页面

        /// <summary>
        /// 用户权限设置tab页面
        /// </summary>
        /// <returns></returns>
        [Description("用户权限设置tab页面")]
        [GetButtonPermissions("permission", "ManageIndex")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult PermissionManage()
        {
            return PartialView();
        }



        #endregion

        #region 用户权限设置dialog页面

        #endregion

        #endregion

        #region 数据

        /// <summary>
        /// 提供用户管理列表信息json数据
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <param name="positionId">岗位ID</param>
        /// <param name="roleId">角色ID</param>
        /// <param name="model">自定义搜索条件模型</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <param name="rangeFilter">是否根据管理范围过滤</param>
        /// <returns></returns>
        [Description("提供用户信息json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult UserManageJson(int? companyId, int? deptId, int? positionId, int? roleId, string model, int? page, int? rows, bool rangeFilter = true)
        {
            int Page = page ?? 1;
            int Rows = rows ?? 10;

            return ServiceFactory.SysUser.UserManageGrid(companyId, deptId, positionId, roleId, model, Page, Rows, rangeFilter);
        }

        /// <summary>
        /// 提供用户菜单权限json数据
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="rangeFilter">是否根据管理范围过滤，默认过滤</param>
        /// <returns></returns>
        [Description("提供用户菜单权限json数据")]
        [CatchException(ActionType.数据)]
        public JsonResult UserMenuPermissionJson(int userId, bool rangeFilter = true)
        {
            return ServiceFactory.SysUserMenuPermission.UserMenuPermissionGrid(userId, rangeFilter);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存用户
        /// </summary>
        /// <param name="model">用户模型</param>
        /// <returns></returns>
        [Description("保存用户")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveUser(UserModel model)
        {
            return ServiceFactory.SysUser.SaveUser(model);
        }

        /// <summary>
        /// 移除用户
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <returns></returns>
        [Description("移除用户")]
        [CatchException(ActionType.操作)]
        public JsonResult RemoveUser(int id)
        {
            return ServiceFactory.SysUser.RemoveUser(id);
        }

        /// <summary>
        /// 重置用户密码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Description("用户密码重置请求")]
        [CatchException(ActionType.操作)]
        public JsonResult ResetPassWord(PasswordModel model)
        {
            return ServiceFactory.SysUser.ResetUserPassword(model);
        }

        /// <summary>
        /// 禁用/启用用户
        /// </summary>
        /// <param name="id">用户id</param>
        /// <param name="disableState">状态true，则启用；false则禁用</param>
        /// <returns></returns>
        [Description("禁用/启用用户")]
        [CatchException(ActionType.操作)]
        public JsonResult EnableOrDisableUser(int id, bool disableState)
        {
            return ServiceFactory.SysUser.EnableOrDisableUser(id, disableState);
        }


        /// <summary>
        /// 保存用户权限
        /// </summary>
        /// <param name="permission">用户权限</param>
        /// <param name="userId">用户ID</param>
        /// <param name="otherUserIds">应用到其他用户的id</param>
        /// <returns></returns>
        [Description("保存用户权限请求")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveUserPermission([ModelBinder(typeof(JsonBinder<PermissionModel>))]IEnumerable<PermissionModel> permission, int userId, string otherUserIds)
        {
            return ServiceFactory.SysUserMenuPermission.SaveUserPermission(permission, userId, otherUserIds);
        }

        #endregion

        #region 其他

        /// <summary>
        /// 检查账号（用户名、登录别名）是否存在。
        /// </summary>
        /// <param name="value">账号值</param>
        /// <param name="key">需要忽略的用户的主键值，默认值为0。新增时判定可不传key参数，更新时判定则key传当前用户的主键</param>
        /// <returns></returns>
        [Login]
        [CatchException(ActionType.忽略异常)]
        public bool CheckAccountNameExist(string value, int key = 0)
        {
            return ServiceFactory.SysUser.CheckAccountNameExist(value, key);
        }

        #endregion
    }
}