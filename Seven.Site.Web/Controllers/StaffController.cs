﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using Seven.DataModel.ViewModels.StaffAbout;
using Seven.Service;
using Seven.Site.Mvc.Attributes;
using Seven.Site.Mvc.Filters;

namespace Seven.Site.Web.Controllers
{
    public class StaffController : BaseController
    {
        #region 页面

        /// <summary>
        /// 员工管理页面
        /// </summary>
        /// <returns></returns>
        [Description("员工管理页面")]
        [GetButtonPermissions]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public ViewResult ManageIndex()
        {
            return View();
        }

        /// <summary>
        /// 新建员工页面
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <param name="deptName">部门名称</param>
        /// <returns></returns>
        [Description("新建员工页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult StaffCreateForm(int companyId, int deptId = 0, string deptName = "")
        {
            var model = ServiceFactory.HrStaff.GetNewStaffModel(companyId, deptId, deptName);

            return PartialView(model);
        }

        /// <summary>
        /// 编辑员工页面
        /// </summary>
        /// <param name="id">员工ID</param>
        /// <returns></returns>
        [Description("编辑员工页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public PartialViewResult StaffEditForm(int id)
        {
            var model = ServiceFactory.HrStaff.GetEditStaffModel(id);

            return PartialView("StaffCreateForm", model);
        }

        #endregion

        #region 数据

        /// <summary>
        /// 提供员工信息json数据
        /// </summary>
        /// <param name="companyId">公司ID</param>
        /// <param name="deptId">部门ID</param>
        /// <param name="positionId">岗位ID</param>
        /// <param name="model">自定义搜索条件模型</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <param name="rangeFilter">是否根据管理范围过滤</param>
        /// <returns></returns>
        [Description("提供员工信息json数据")]
        [HttpPost]
        [CatchException(ActionType.数据)]
        public JsonResult StaffJson(int? companyId, int? deptId, int? positionId, string model, int? page, int? rows, bool rangeFilter = true)
        {
            int Page = page ?? 1;
            int Rows = rows ?? 10;

            return ServiceFactory.HrStaff.StaffGrid(companyId, deptId, positionId, model, Page, Rows);
        }

        /// <summary>
        /// 提供性别json数据
        /// </summary>
        /// <returns></returns>
        [Description("提供性别json数据")]
        [Login]
        [CatchException(ActionType.数据)]
        public JsonResult SexJson()
        {
            return ServiceFactory.HrStaff.SexJson();
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存员工
        /// </summary>
        /// <param name="model">员工模型</param>
        /// <returns></returns>
        [Description("保存员工")]
        [CatchException(ActionType.操作)]
        public JsonResult SaveStaff(StaffModel model)
        {
            return ServiceFactory.HrStaff.SaveStaff(model);
        }

        /// <summary>
        /// 移除员工
        /// </summary>
        /// <param name="id">员工ID</param>
        /// <returns></returns>
        [Description("移除员工")]
        [CatchException(ActionType.操作)]
        public JsonResult RemoveStaff(int id)
        {
            return ServiceFactory.HrStaff.RemoveStaff(id);
        }

        #endregion

        #region 其他

        #endregion
    }
}