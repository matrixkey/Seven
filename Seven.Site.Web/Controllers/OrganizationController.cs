﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using Seven.Site.Mvc.Attributes;

namespace Seven.Site.Web.Controllers
{
    public class OrganizationController : BaseController
    {
        /// <summary>
        /// 组织结构展示页面
        /// </summary>
        /// <returns></returns>
        [Description("组织结构展示页面")]
        [ViewPage]
        [CatchException(ActionType.视图)]
        public ViewResult Index()
        {
            return View();
        }
    }
}