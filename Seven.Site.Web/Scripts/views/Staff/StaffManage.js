﻿/*
==============================================================================
//  员工管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Staff.StaffManage");

    var tree = "#tree", dg = "#dg"; var queryFm = "#queryFm";

    window.views.Staff.StaffManage.initPage = function (permissions) {
        var data = window.jeasyui.helper.buildToolbarUsePermissionJson(permissions, true);
        data.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.Staff.reloadStaff(); } } });

        var treeInit = function () {

            var options = {
                url: "/Public/OrganizationJson",
                animate: true,
                onSelect: function (node) {
                    if (node.attributes.IsCompany) {
                        $(dg).datagrid("load", { companyId: node.id, model: $(queryFm).serialize() });
                    }
                    else if (node.attributes.IsDept) {
                        $(dg).datagrid("load", { deptId: node.id, model: $(queryFm).serialize() });
                    }
                },
                onBeforeLoad: function (node, param) {
                    $.easyui.loading({ locale: $(this).currentRegion(), msg: "数据加载中..." });
                },
                onLoadSuccess: function (node) {
                    $.easyui.loaded($(this).currentRegion());
                    if (node == null) {
                        $(this).tree("expandAll");
                    }
                }
            };

            $(tree).tree(options);
        };

        var bindQueryControl = function () {

            $("#Birthday1", queryFm).addClass("Wdate").click(function () {
                WdatePicker({
                    readOnly: true,
                    dateFmt: "yyyy-MM-dd",
                    maxDate: "#F{$dp.$D('Birthday2')}"
                });
            });
            $("#Birthday2", queryFm).addClass("Wdate").click(function () {
                WdatePicker({
                    readOnly: true,
                    dateFmt: "yyyy-MM-dd",
                    minDate: "#F{$dp.$D('Birthday1')}"
                });
            });
        };

        var bindQueryButtonEvent = function () {
            $("#aquery", queryFm).click(function () {
                var queryParam = { model: $(queryFm).serialize() };
                var selected = $(tree).tree("getSelected");
                if (selected) {
                    if (selected.attributes.IsCompany) { queryParam.companyId = selected.id; }
                    else if (selected.attributes.IsDept) { queryParam.deptId = selected.id; }
                };
                $(dg).datagrid('load', queryParam);
            });
        };

        var dataGridInit = function () {

            var toolbar = $("<div />").appendTo("body").toolbar({
                data: data
            }).toolbar("toolbar");

            var frozenColumns = function (showCheckBox) {
                var result = [];
                if (showCheckBox) {
                    result.push({ field: 'ck', checkbox: true });
                }
                var normal = [{ field: 'RealName', title: '姓名', width: 100 }, { field: 'UserName', title: '用户名', width: 100 }];
                $.util.merge(result, normal);
                return result;
            };

            var columns = function (sortableEnable) {
                var result = [];
                var normal = [
                        { field: 'CompanyName', title: '公司', width: 100 },
                        { field: 'DeptName', title: '部门', width: 100 },
                        { field: 'Sex', title: '性别', width: 100 },
                        { field: 'MobilePhoneNumber', title: '手机号码', width: 100 },
                        { field: 'State', title: '状态', width: 100 }
                ];
                if (sortableEnable) {
                    $.array.forEach(normal, function (val, index, array) {
                        $.extend(val, { sortable: true });
                    });
                }
                $.util.merge(result, normal);
                return result;
            };

            var options = {
                url: "/Staff/StaffJson",
                queryParams: { first: true, rangeFilter: true },
                idField: "ID",
                toolbar: toolbar,
                rownumbers: false,
                nowrap: true,
                striped: true,
                fit: true,
                border: false,
                remoteSort: false,
                singleSelect: true,
                checkOnSelect: false,
                selectOnCheck: false,
                frozenColumns: [frozenColumns(false)],
                columns: [columns(true)],
                pagination: true,
                onBeforeLoad: function (param) {
                    return !param.first;
                }
            };

            $(dg).datagrid(options);
            window.jeasyui.helper.drawTopBorderForDataGridTools($(dg));
        };

        treeInit();
        bindQueryControl();
        bindQueryButtonEvent();
        dataGridInit();
    };

    window.views.Staff.createStaff = function () {
        var selectNode = $(tree).tree("getSelected");
        if (!selectNode || !selectNode.attributes.IsDept) { $.easyui.messager.alert("请选择部门！"); return; }

        var param = $.param({ companyId: selectNode.attributes.CompanyID, deptId: selectNode.id, deptName: selectNode.text });
        $.easyui.showDialog({
            title: "新建员工",
            href: "/Staff/StaffCreateForm?" + param,
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    window.views.Staff.saveStaff(model);
                } else { return false; }
            },
            width: 850,
            height: 280
        });
    };

    window.views.Staff.editStaff = function (row) {
        row = row ? row : $(dg).datagrid("getSelected");
        if (row) {
            $.easyui.showDialog({
                title: "编辑员工 " + row.RealName,
                href: "/Staff/StaffEditForm?id=" + row.ID,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.Staff.saveStaff(model);
                    } else { return false; }
                },
                width: 850,
                height: 280
            });
        }
    };

    window.views.Staff.saveStaff = function (model) {
        $.easyui.loading({ locale: window.top.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Staff/SaveStaff", model, function (result) {
            $.easyui.loaded(window.top.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Staff.reloadStaff);
        }, 'json');
    };

    window.views.Staff.removeStaff = function (row) {
        row = row == undefined ? $(dg).datagrid("getSelected") : row;
        if (row) {
            var _callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/Staff/RemoveStaff", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBackWhenRemove.call(this, result, function () { $(dg).datagrid("deleteRow", $(dg).datagrid("getRowIndex", row)); });
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.RealName + " 吗?", _callback);
        }
    };

    window.views.Staff.reloadStaff = function () {
        $(dg).datagrid('clearSelections');
        //$(dg).datagrid('clearChecked');
        $(dg).datagrid("reload");
    };

})(jQuery);