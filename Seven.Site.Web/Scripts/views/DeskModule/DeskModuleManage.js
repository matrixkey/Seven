﻿

(function ($) {

    $.util.namespace("views.DeskModule.DeskModuleManage");

    var layout1 = "#layout", datagrid1 = "#dg-left", datagrid2 = "#dg-right";
    var currentArgument = {};
    window.views.DeskModule.DeskModuleManage.initPage = function (permissions, left, right) {
        currentArgument.left = left; currentArgument.right = right;
        var data = window.jeasyui.helper.buildToolbarUsePermissionJson(permissions, true);
        data.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.DeskModule.reloadDeskModule(); } } });

        var layoutResize = function () {
            var size = $.util.windowSize();
            var layout = $(layout1);
            var width = parseInt(size.width - 70) / 2;
            //以下对layout的resize方式，在遨游浏览器中无效
            //var west = layout.layout("panel", "west");
            //var east = layout.layout("panel", "east");
            //west.panel("options").width = width;
            //east.panel("options").width = width;
            //layout.layout("resize");

            //以下写法，先设置west、east的宽度，再用js方式去初始化layout，兼容性好
            var west = $("#left", layout).width(width);
            var east = $("#right", layout).width(width);
            layout.layout({ fit: true, border: false });
        };

        var toolBarInit = function () {
            var center1 = $(layout1).layout("panel", "center");
            $.each(data, function (index, t) {
                if (t.type && t.type == "button") {
                    var opts = t.options;
                    opts.plain = true; opts.onClick = opts.onclick;
                    $("<a />").linkbutton(opts).appendTo(center1);
                    $("<br /><br />").appendTo(center1);
                }
            });
        };

        var dblDataGridInit = function () {

            var frozenColumns = function (showCheckBox) {
                var result = [];
                if (showCheckBox) {
                    result.push({ field: 'ck', checkbox: true });
                }
                var normal = [{ field: 'Name', title: '名称', width: 100 }];
                $.util.merge(result, normal);
                return result;
            };

            var columns = function (sortableEnable) {
                var result = [];
                var normal = [
                        { field: 'SortNumber', title: '排序号', width: 80 },
                        { field: 'MenuName', title: '关联菜单', width: 90 },
                        { field: 'ControlType', title: '控制类型', width: 90 },
                        {
                            field: 'ReceiveMessage', title: '是否接收消息', width: 110,
                            formatter: function (val) {
                                return val ? "<b>接收</b>" : "<b>不接收</b>";
                            },
                            styler: function (val) { return val ? window.comlib.stateColor.green : window.comlib.stateColor.red; }
                        },
                        {
                            field: 'IsDisabled', title: '是否禁用', width: 90,
                            formatter: function (val) {
                                return val ? "<b>禁用</b>" : "<b>启用</b>";
                            },
                            styler: function (val) { return val ? window.comlib.stateColor.red : window.comlib.stateColor.green; }
                        }
                ];
                if (sortableEnable) {
                    $.array.forEach(normal, function (val, index, array) {
                        val.sortable = true;
                    });
                }
                $.util.merge(result, normal);
                return result;
            };

            var options1 = {
                url: "/DeskModule/DeskModuleJson",
                queryParams: { location: "left" },
                idField: "ID",
                title: "左边模块",
                rownumbers: true,
                nowrap: true,
                striped: true,
                fit: true,
                //toolbar: toolbar,
                border: false,
                remoteSort: false,
                singleSelect: true,
                frozenColumns: [frozenColumns(false)],
                columns: [columns(true)],
                pagination: false,
                onSelect: function (rowIndex, rowData) {
                    $(datagrid2).datagrid('clearSelections');
                }
            };

            var options2 = {
                url: "/DeskModule/DeskModuleJson",
                queryParams: { location: "right" },
                idField: "ID",
                title: "右边模块",
                rownumbers: true,
                nowrap: true,
                striped: true,
                fit: true,
                //toolbar: toolbar,
                border: false,
                remoteSort: false,
                singleSelect: true,
                frozenColumns: [frozenColumns(false)],
                columns: [columns(true)],
                pagination: false,
                onSelect: function (rowIndex, rowData) {
                    $(datagrid1).datagrid('clearSelections');
                }
            };

            $(datagrid1).datagrid(options1); $(datagrid2).datagrid(options2);
        };

        layoutResize();
        toolBarInit();
        dblDataGridInit();
    };

    window.views.DeskModule.createDeskModule = function () {
        $.easyui.showDialog({
            title: "新建桌面模块",
            href: "/DeskModule/DeskModuleCreateForm",
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    window.views.DeskModule.saveDeskModule(model);
                } else { return false; }
            },
            width: 850,
            height: 370
        });
    };

    window.views.DeskModule.editDeskModule = function () {
        var row = $(datagrid1).datagrid("getSelected") || $(datagrid2).datagrid("getSelected");
        if (row) {
            $.easyui.showDialog({
                title: "编辑桌面模块 " + row.Name,
                href: "/DeskModule/DeskModuleEditForm?id=" + row.ID,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.DeskModule.saveDeskModule(model);
                    } else { return false; }
                },
                width: 850,
                height: 370
            });
        }
    };

    window.views.DeskModule.saveDeskModule = function (model) {
        $.easyui.loading({ locale: window.top.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/DeskModule/SaveDeskModule", model, function (result) {
            $.easyui.loaded(window.top.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, function () { window.views.DeskModule.reloadDeskModule(model.ID, model.OriginalLocation, model.Location); });
        }, 'json');
    };

    window.views.DeskModule.removeDeskModule = function () {
        var row = $(datagrid1).datagrid("getSelected") || $(datagrid2).datagrid("getSelected");
        if (row) {
            var _callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/DeskModule/RemoveDeskModule", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, function () {
                        if (row.Location == currentArgument.left) {
                            $(datagrid1).datagrid("deleteRow", $(datagrid1).datagrid("getRowIndex", row));
                        }
                        else if (row.Location == currentArgument.right) {
                            $(datagrid2).datagrid("deleteRow", $(datagrid2).datagrid("getRowIndex", row));
                        }
                    });
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.Name + " 吗?", _callback);
        }
    };

    window.views.DeskModule.reloadDeskModule = function (id, orignalLocation, location) {
        if (arguments.length > 0 && ((id == "0" && location == currentArgument.left) || (id != "0" && (location == currentArgument.left) && (orignalLocation == currentArgument.left)))) {
            $(datagrid1).datagrid('clearSelections');
            $(datagrid1).datagrid("reload");
        }
        else if (arguments.length > 0 && ((id == "0" && location == currentArgument.right) || (id != "0" && (location == currentArgument.right) && (orignalLocation == currentArgument.right)))) {
            $(datagrid2).datagrid('clearSelections');
            $(datagrid2).datagrid("reload");
        }
        else {
            //一次性刷新
            $.post("/DeskModule/DeskModuleJsonForOnce", function (data) {
                if (data.left) {
                    $(datagrid1).datagrid('clearSelections');
                    $(datagrid1).datagrid("loadData", data.left);
                }
                if (data.right) {
                    $(datagrid2).datagrid('clearSelections');
                    $(datagrid2).datagrid("loadData", data.right);
                }
            });
        }
    };

    window.views.DeskModule.deployPermission = function () {
        $.easyui.showDialog({
            title: "配置桌面模块权限",
            href: "/DeskModule/PermissionManage",
            enableSaveButton: false, enableApplyButton: false,
            width: 600,
            height: 470
        });
    };

})(jQuery);