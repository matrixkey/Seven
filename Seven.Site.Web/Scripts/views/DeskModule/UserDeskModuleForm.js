﻿/*
==============================================================================
//  用户桌面模块新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.DeskModule.UserDeskModuleForm");

    window.views.DeskModule.UserDeskModuleForm.initPage = function (form) {

        var _form = $("#" + form);

        var _key = $("#ID", _form).val();

        var _controlInit = function () {

            $("#SortNumber", _form).numberbox({
                required: true,
            });

            $("#Location", _form).combobox({
                url: "/DeskModule/LocationJson",
                valueField: "Value",
                textField: "Text",
                editable: false,
                required: true
            });

            $("#Height", _form).numberbox({
                required: true,
                min: 100,
                max: 600
            });

            $("#Size", _form).numberbox({
                required: true,
                min: 5,
                max: 25
            });

            $("#Remark", _form).attr(window.comlib.textAreaAttr);
        };

        var _bindButtonEvent = function () {
            $("#select_menu", _form).click(function () {
                var el = $("#MenuID", _form);
                window.comlib.showMenuSelector(function (node) {
                    if (node) {
                        el.val(node.id);
                        el.next().text(node.text);
                    }
                }, el.val(), 0, false);
            });
        };

        _controlInit();
        _bindButtonEvent();
    };

})(jQuery);