﻿/*
==============================================================================
//  菜单权限新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Menu.MenuPermissionForm");

    window.views.Menu.MenuPermissionForm.initPage = function (form, menuId, buttonType) {

        var _form = $("#" + form);

        var _key = $("#ID", _form).val();

        var _controlInit = function () {
            $("#Name", _form).validatebox({
                required: true
            });

            $("#Type", _form).combobox({
                url: "/Menu/PermissionTypeJson",
                valueField: "Value",
                textField: "Text",
                editable: false,
                required: true,
                onChange: function (newValue, oldValue) {
                    if (newValue == buttonType) {
                        $("#button", _form).show();
                        $("#ButtonName", _form).validatebox("enableValidation");
                    }
                    else {
                        $("#button", _form).hide();
                        $("#ButtonName", _form).validatebox("disableValidation");
                    }
                },
                onLoadSuccess: function () {
                    var data = $(this).combobox("getData");
                    if (data.length == 0) {
                        $(this).combobox("setValue", "");
                    }
                    var defaultValue = $(this).combobox("getValue");
                    if (defaultValue == buttonType) {
                        $("#button", _form).show();
                        $("#ButtonName", _form).validatebox("enableValidation");
                    }
                    else {
                        $("#button", _form).hide();
                        $("#ButtonName", _form).validatebox("disableValidation"); }
                }
            });

            $("#ButtonName", _form).validatebox({ required: true });

            $("#SortNumber", _form).numberbox({
                required: true
            });

            $("#FunctionIDs", _form).combobox({
                url: "/Menu/MenuFunctionJson1",
                valueField: "ID",
                textField: "Name",
                width: 300,
                required: true,
                editable: false,
                multiple: true,
                onBeforeLoad: function (param) {
                    param.menuId = menuId;
                },
                onLoadSuccess: function () {
                    var defaultValue = $(this).combobox("getValues");
                    if (defaultValue.length && defaultValue.length > 0) {
                        $("#FunctionIDs", _form).combobox("setValues", defaultValue[0].split(','));
                    }
                }
            });

            $("#Remark", _form).attr(window.comlib.textAreaAttr);

        };

        var _bindButtonEvent = function () {
            $("#clear_LinkAction", _form).click(function () {
                $("#FunctionIDs", _form).combobox("setValues", []);
            });
        };

        _controlInit();
        _bindButtonEvent();
    };

})(jQuery);