﻿/*
==============================================================================
//  菜单功能新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Menu.MenuFunctionForm");

    window.views.Menu.MenuFunctionForm.initPage = function (form, controller, menuID) {

        var _form = $("#" + form);

        var _key = $("#ID", _form).val();

        var _controlInit = function () {
            $("#Name", _form).validatebox({
                required: true
            });

            $("#Type", _form).combobox({
                url: "/Menu/FunctionTypeJson",
                valueField: "Value",
                textField: "Text",
                editable: false,
                required: true,
                onLoadSuccess: function () {
                    var defaultValue = $(this).combobox("getValue");
                    if (defaultValue == "0") {
                        var data = $(this).combobox("getData");
                        if (data.length > 0) {
                            $(this).combobox("setValue", data[0].Value);
                        }
                        else {
                            $(this).combobox("setValue", "");
                        }
                    }
                }
            });

            $("#SortNumber", _form).numberbox({
                required: true
            });

            $("#ActionName", _form).combobox({
                url: "/Public/ActionJson",
                valueField: "id",
                textField: "text",
                width: 200,
                editable: false,
                required: true,
                onBeforeLoad: function (param) {
                    param.controllerName = controller;
                },
                onChange: function (newValue, oldValue) {
                    $("span#url", _form).text("/" + controller + "/" + newValue);
                    var record = $(this).combobox("getItem", newValue);
                    $("#ActionDescription", _form).val(record.attributes.Description);
                },
                onLoadSuccess: function () {
                    var defaultValue = $(this).combobox("getValue");
                    if ($.string.isNullOrWhiteSpace(defaultValue)) {
                        var data = $(this).combobox("getData");
                        if (data.length > 0) {
                            $(this).combobox("setValue", data[0].id);
                            $("span#url", _form).text("/" + controller + "/" + data[0].id);
                            $("#ActionDescription", _form).val(data[0].attributes.Description);
                        }
                        else {
                            var oldText = $("span#url", _form).text();
                            $("span#url", _form).text(oldText.substring(0, oldText.lastIndexOf("/") + 1) + "无效Action");
                        }
                    }
                    else {
                        $(this).combobox("setValue", defaultValue);
                        $("span#url", _form).text("/" + controller + "/" + defaultValue);
                        var record = $(this).combobox("getItem", defaultValue);
                        if (record != null) { $("#ActionDescription", _form).val(record.attributes.Description); }
                    }
                }
            });

            $("#Remark", _form).attr(window.comlib.textAreaAttr);

        };

        var _bindButtonEvent = function () {
            
        };

        _controlInit();
        _bindButtonEvent();
    };

})(jQuery);