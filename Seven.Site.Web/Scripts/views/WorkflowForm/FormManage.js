﻿/*
==============================================================================
//  表单类型管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.WorkflowForm.FormManage");

    var tree = "#tree", dg = "#dg";

    window.views.WorkflowForm.FormManage.initPage = function (permissions, typeGroup, formGroup) {
        var data = window.jeasyui.helper.buildToolbarUsePermissionJson(permissions, true, true);

        var initTreeButton = function () {
            var data1 = window.jeasyui.helper.splitToolbar(data, typeGroup);
            data1.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.WorkflowForm.reloadFormType(); } } });
            var toolbar = $("<div />").insertBefore(tree).toolbar({
                data: data1
            }).toolbar("toolbar");
        };

        var treeInit = function () {

            var options = {
                url: "/WorkflowForm/FormTypeJson",
                parentField: "ParentID",
                animate: true,
                onSelect: function (node) {
                    $(dg).datagrid("load", { typeId: node.id });
                },
                onBeforeLoad: function (node, param) {
                    $.easyui.loading({ locale: $(this).currentRegion(), msg: "数据加载中..." });
                },
                onLoadSuccess: function (node) {
                    $.easyui.loaded($(this).currentRegion());
                }
            };

            $(tree).tree(options);
        };

        var dataGridInit = function () {
            var data2 = window.jeasyui.helper.splitToolbar(data, formGroup);
            data2.push({
                type: "button", options: {
                    text: "刷新", iconCls: "icon-reload", onclick: function (t) {
                        window.views.WorkflowForm.reloadForm();
                    }
                }
            });
            var toolbar = $("<div />").appendTo("body").toolbar({
                data: data2
            }).toolbar("toolbar");

            var frozenColumns = function (showCheckBox) {
                var result = [];
                if (showCheckBox) {
                    result.push({ field: 'ck', checkbox: true });
                }
                var normal = [{ field: 'Name', title: '表单名称', width: 150 }];
                $.util.merge(result, normal);
                return result;
            };

            var columns = function (sortableEnable) {
                var result = [];
                var normal = [
                        { field: 'SortNumber', title: '排序号', width: 100 },
                        { field: 'CreateUserRealName', title: '创建人', width: 100 },
                        { field: 'Remark', title: '备注', width: 150 }
                ];
                if (sortableEnable) {
                    $.array.forEach(normal, function (val, index, array) {
                        val.sortable = true;
                    });
                }
                $.util.merge(result, normal);
                return result;
            };

            var options = {
                url: "/WorkflowForm/FormJson",
                queryParams: { first: true },
                idField: "ID",
                title: "表单列表",
                toolbar: toolbar,
                rownumbers: false,
                nowrap: true,
                striped: true,
                fit: true,
                border: false,
                remoteSort: false,
                singleSelect: true,
                frozenColumns: [frozenColumns(false)],
                columns: [columns(true)],
                pagination: true,
                onBeforeLoad: function (param) {
                    return !param.first;
                }
            };

            $(dg).datagrid(options);
        };

        initTreeButton();
        treeInit();
        dataGridInit();
    };

    window.views.WorkflowForm.createFormType = function () {
        var selected = $(tree).tree("getSelected");
        var param = selected ? { parentId: selected.id } : { parentId: 0 };
        $.easyui.showDialog({
            title: "新建表单类型",
            href: "/WorkflowForm/FormTypeCreateForm?" + $.param(param),
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    if (model.CompanyID == 0) { $.easyui.messager.alert("请选择表单类型的所属公司！"); return false; }
                    window.views.WorkflowForm.saveFormType(model);
                } else { return false; }
            },
            width: 850,
            height: 280,
            topMost: true
        });
    };

    window.views.WorkflowForm.editFormType = function () {
        var selected = $(tree).tree("getSelected");
        if (selected) {
            $.easyui.showDialog({
                title: "编辑表单类型",
                href: "/WorkflowForm/FormTypeEditForm?id=" + selected.id,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.WorkflowForm.saveFormType(model);
                    } else { return false; }
                },
                width: 850,
                height: 280,
                topMost: true
            });
        }
    };

    window.views.WorkflowForm.saveFormType = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/WorkflowForm/SaveFormType", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBackForTree(result, $(tree));
        }, 'json');
    };

    window.views.WorkflowForm.removeFormType = function () {
        var selected = $(tree).tree("getSelected");
        if (selected) {
            var _callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/WorkflowForm/RemoveFormType", { id: selected.id }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, function () { $(tree).tree("remove", selected.target); });
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + selected.text + " 吗?", _callback);
        }
    };

    window.views.WorkflowForm.reloadFormType = function () {
        var t = $(tree);
        var oldNode = t.tree("getSelected");
        if (oldNode) {
            var opts = t.tree("options"), url = opts.url;
            $.post(url, function (result) {
                t.tree("loadData", result);
                var tempNode = t.tree("find", oldNode.id);
                if (tempNode) {
                    t.tree("select", tempNode.target);
                }
            });
        }
        else {
            t.tree("reload");
        }
    };

    window.views.WorkflowForm.createForm = function () {
        var selected = $(tree).tree("getSelected");
        if (selected) {
            var param = { typeId: selected.id };
            $.easyui.showDialog({
                title: "新建表单",
                href: "/WorkflowForm/FormCreateForm?" + $.param(param),
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.WorkflowForm.saveForm(model);
                    } else { return false; }
                },
                width: 850,
                height: 250,
                topMost: true
            });
        }
    };

    window.views.WorkflowForm.editForm = function () {
        var row = $(dg).datagrid("getSelected");
        if (row) {
            $.easyui.showDialog({
                title: "编辑表单",
                href: "/WorkflowForm/FormEditForm?id=" + row.ID,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.WorkflowForm.saveForm(model);
                    } else { return false; }
                },
                width: 850,
                height: 250,
                topMost: true
            });
        }
    };

    window.views.WorkflowForm.saveForm = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/WorkflowForm/SaveForm", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack(result, function () {
                window.views.WorkflowForm.reloadForm();
                if ($.string.toBoolean(model.OpenDesignPage) == true) {
                    window.views.WorkflowForm.designForm(result.data.id);
                }
            });
        }, 'json');
    };

    window.views.WorkflowForm.removeForm = function () {
        var row = $(dg).datagrid("getSelected");
        if (row) {
            var _callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/WorkflowForm/RemoveForm", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.WorkflowForm.reloadForm);
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.Name + " 吗?", _callback);
        }
    };

    window.views.WorkflowForm.designForm = function (formId) {
        if (formId == undefined) {
            var row = $(dg).datagrid("getSelected");
            if (row) { formId = row.ID; }
        }
        if (formId != undefined) { window.comlib.windowOpen("/WorkflowForm/FormDesignForm?id=" + formId); }
    };

    window.views.WorkflowForm.saveFormDesign = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/WorkflowForm/SaveFormDesign", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack(result, function () {

            });
        }, 'json');
    };

    window.views.WorkflowForm.reloadForm = function () {
        $(dg).datagrid('clearSelections');
        //$(dg).datagrid('clearChecked');
        $(dg).datagrid("reload");
    };

})(jQuery);