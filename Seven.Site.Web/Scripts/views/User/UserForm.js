﻿/*
==============================================================================
//  用户新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.User.UserForm");

    window.views.User.UserForm.initPage = function (form, rangeCompanyId, rangeDeptId) {

        var _form = $("#" + form);

        var _key = $("#ID", _form).val(); var create = _key == "0"; var staffCompanyId = rangeCompanyId;

        var _controlInit = function () {
            if (create) {
                $("#UserName", _form).validatebox({
                    required: true,
                    validType: ['code', 'ajaxAddValidWithoutField["用户名","/User/CheckAccountNameExist"]']
                });
            }
            else {
                $("#UserName", _form).addClass("BigStatic").prop("readonly", true);
            }

            $("#RealName", _form).searchbox({
                searcher: function (value) {
                    var selected = $("#StaffID", _form).val();
                    var onEnterClick = function (selected) {
                        if (selected) {
                            $("#StaffID", _form).val(selected.ID);
                            $("#RealName", _form).searchbox("setValue", selected.RealName);
                            staffCompanyId = selected.CompanyID;
                        } else {
                            $.easyui.messager.alert("提示", "请先选择一行", "warning");
                            return false;
                        }
                    };
                    if (create) {
                        window.comlib.showStaffSelector(onEnterClick, selected, rangeCompanyId, rangeDeptId);
                    }
                }
            });
            $("#RealName", _form).next(".searchbox").find("input").addClass("BigStatic").prop("readonly", true);
            $("#RealName", _form).next(".searchbox").find("input").validatebox({ required: true });

            if (create) {
                $("#LoginCode", _form).validatebox({
                    validType: ['code', 'ajaxAddValidWithoutField["登录别名","/User/CheckAccountNameExist"]']
                });
            }
            else {
                $("#LoginCode", _form).validatebox({
                    validType: ['code', 'ajaxUpdateValidWithoutField["登录别名","/User/CheckAccountNameExist","' + _key + '"]']
                });
            }

            $("#RoleName", _form).searchbox({
                searcher: function (value) {
                    var selected = $("#RoleID", _form).val();
                    var onEnterClick = function (selected) {
                        if (selected) {
                            $("#RoleID", _form).val(selected.ID);
                            $("#RoleName", _form).searchbox("setValue", selected.Name);
                        } else {
                            $.easyui.messager.alert("提示", "请先选择一行", "warning");
                            return false;
                        }
                    };

                    if (staffCompanyId == "0") {
                        $.messager.alert("提示", "请先选择员工", "warning");
                    }
                    else { window.comlib.showRoleSelector(onEnterClick, selected, staffCompanyId); }
                }
            });
            $("#RoleName", _form).next(".searchbox").find("input").addClass("BigStatic").prop("readonly", true);
            $("#RoleName", _form).next(".searchbox").find("input").validatebox({ required: true });

            if (create) {
                $("#PassWord", _form).validatebox({
                    required: true,
                    validType: ["passwordNew"]
                });

                $("#PassWordRepeat", _form).validatebox({
                    required: true,
                    validType: ["equals['PassWord','id']"]
                });
            }
            else {
                $("#PassWord,#PassWordRepeat", _form).prop("disabled", true);
                $("#PassWord", _form).parents("tr:first").hide();
            }

            $("#Remark", _form).attr(window.comlib.textAreaAttr);
        };

        var _bindButtonEvent = function () {

        };

        _controlInit();
        _bindButtonEvent();
    };

})(jQuery);