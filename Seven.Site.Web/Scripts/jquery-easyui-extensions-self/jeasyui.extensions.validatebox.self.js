﻿/**
* jQuery EasyUI 1.3.3
* Copyright (c) 2009-2013 www.jeasyui.com. All rights reserved.
*
* Licensed under the GPL or commercial licenses
* To use it on other terms please contact us: jeasyui@gmail.com
* http://www.gnu.org/licenses/gpl.txt
* http://www.jeasyui.com/license_commercial.php
*
* jQuery EasyUI validatebox Extensions 1.0 beta
* jQuery EasyUI validatebox 组件扩展
* jeasyui.extensions.validatebox.js
* 开发 李溪林
* 最近更新：2013-07-31
*
* 依赖项：
*   1、jquery.jdirk.js v1.0 beta late
*   2、jeasyui.extensions.js v1.0 beta late
*
* Copyright (c) 2013 Lixilin personal All rights reserved.
* http://www.chenjianwei.org
*/

/*
功能说明：
*/
(function ($, undefined) {

    var rules = {
        //这种验证不需要后台用sql查询，参数不传入 查询的目标字段，只传入查询的目标字段的值，后台的参数名只能是Value
        ajaxAddValidWithoutField: {
            validator: function (value, param) {
                var url = param[1];         //提交地址
                var va = value;             //验证值
                var pa = {
                    Value: va
                };
                var result = $.util.requestAjaxBoolean(url, pa);
                return !result;
            },
            message: '{0}有重复记录'
        },
        //这种验证不需要后台用sql查询，参数不传入 查询的目标字段，只传入查询的目标字段的值，注意2个参数的次序，后台的参数名只能是Value和Key
        ajaxUpdateValidWithoutField: {
            validator: function (value, param) {
                var url = param[1];         //提交地址
                var va = value;             //验证值
                var key = param[2];
                var pa = {
                    Key: key,
                    Value: va
                };
                var result = $.util.requestAjaxBoolean(url, pa);
                return !result;
            },
            message: '{0}有重复记录'
        },
        //  必须是安全的密码字符(由字符或数字或特殊字符(~!@#$%^&*.)组成， 6-16 位)格式
        passwordNew: {
            validator: function (value) {
                str = $.string.isNullOrEmpty(value) ? "" : String(value);
                return (/^[\@A-Za-z0-9\!\#\$\%\^\&\*\.\~]{6,16}$/.test(str));
            },
            message: "输入的内容必须是安全的密码字符(由字符、数字或特殊字符(~!@#$%^&*.)组成，6 至 16 位)格式."
        }
    };
    $.extend($.fn.validatebox.defaults.rules, rules);




})(jQuery);