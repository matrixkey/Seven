﻿
CKEDITOR.editorConfig = function (config) {

    config.language = 'zh-cn';//语言包
    config.skin = 'moono';//皮肤
    config.font_names = '宋体/宋体;黑体/黑体;仿宋/仿宋_GB2312;楷体/楷体_GB2312;隶书/隶书;幼圆/幼圆;微软雅黑/微软雅黑;' + config.font_names;
    config.resize_enabled = false;// 取消 “拖拽以改变尺寸”功能
    config.allowedContent = true;
    config.toolbar = "matrixkey";
    config.toolbar_matrixkey = [
        { name: 'document', items: ['Source', '-', 'sevenPreview'] },
        { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
        { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll'] },
        { name: 'insert', items: ['HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'] },
        '/',
        { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
        { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
        '/',
        { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
        { name: 'colors', items: ['TextColor', 'BGColor'] },
        { name: 'tools', items: ['Maximize', 'ShowBlocks', '-', 'sevenHelp'] }
    ];

    config.extraPlugins = 'seven_help,seven_preview,seven_controls';//自定义的扩展插件，放在plugins文件夹中
    config.removePlugins = "forms";//移除自带的form插件，防止与seven_controls中的各控件冲突,
    config.contentsCss = ['/Scripts/jquery-plugins/My97DatePicker/skin/WdatePicker.css','/Scripts/jquery-plugins/ckeditor/plugins/seven_controls/seven_controls.css']; //加载my97等样式
};