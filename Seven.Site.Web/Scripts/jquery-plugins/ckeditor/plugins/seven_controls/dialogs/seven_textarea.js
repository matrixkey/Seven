﻿(function () {

    var dialogName = "seven_textarea";

    CKEDITOR.dialog.add(dialogName, function (editor) {

        //常规属性的赋值，如value、size、maxLength、title
        var autoSetup1 = function (element) {
            var value = element.getAttribute(this.id) || "";
            this.setValue(value);
        };

        //单位是px的样式属性的赋值
        var autoSetup2 = function (element) {
            var value = CKEDITOR.tools.convertToPx(element.getStyle(this.id)) || "";
            this.setValue(value);
        };

        //单位不是px的样式属性的赋值
        var autoSetup3 = function (element) {
            var value = element.getStyle(this.id) || "";
            this.setValue(value);
        };

        //常规属性的提交，如value、title
        var autoCommit1 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setAttribute(this.id, value); }
            else
            { element.removeAttribute(this.id); }
        };

        //单位是px的样式属性的提交
        var autoCommit2 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setStyle(this.id, CKEDITOR.tools.cssLength(value)); }
            else
            { element.removeStyle(this.id); }
        };

        //单位不是px的样式属性的提交
        var autoCommit3 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setStyle(this.id, value); }
            else
            { element.removeStyle(this.id); }
        };

        var htmlAttributes = ["title", "class"];//value不在其中，textarea的value特殊，需要自定义事件
        var pxCssAttributes = ["width", "height", "font-size"];
        var nopxCssAttributes = ["text-align"];

        var onShow = function () {
            delete this.textArea;

            var element = editor.getSelection().getSelectedElement();
            if (element && element.getName() == "textarea") {
                this.textArea = element;
                this.setupContent(element);
            }
        };

        var onOk = function () {
            var element = this.textArea, isInsertMode = !element;
            if (isInsertMode) {
                element = editor.document.createElement("textarea");
                editor.insertElement(element);
            }
            this.commitContent({ element: element }); //此处的参数传入方式，决定了element的commit属性值的参数签名
        };

        var onLoad = function () {
            this.foreach(function (contentObj) {
                if (htmlAttributes.contains(contentObj.id)) {
                    contentObj.setup = autoSetup1;
                    contentObj.commit = autoCommit1;
                }
                else if (pxCssAttributes.contains(contentObj.id)) {
                    contentObj.validate = CKEDITOR.dialog.validate.integer("请输入数字格式。");
                    contentObj.setup = autoSetup2;
                    contentObj.commit = autoCommit2;
                }
                else if (nopxCssAttributes.contains(contentObj.id)) {
                    contentObj.setup = autoSetup3;
                    contentObj.commit = autoCommit3;
                }
            });
        };

        var controlOptions = {
            id: "info",
            label: "多行文本属性",
            title: "多行文本属性",
            elements: [
                {
                    type: "hbox", widths: ["50%", "50%"],
                    children: [
                        { id: "title", type: "text", label: "控件名称", "default": "" },
                        { id: "font-size", type: "text", label: "字体大小", "default": "12" }
                    ]
                },
                {
                    type: "hbox", widths: ["50%", "50%"],
                    children: [
                        { id: "width", type: "text", label: "控件宽度", "default": "200" },
                        { id: "height", type: "text", label: "控件高度", "default": "60" }
                    ]
                },
                {
                    id: "value", type: "textarea", label: "初始值", "default": "",
                    setup: function (element) { this.setValue(element.$.defaultValue); },
                    commit: function (data) {
                        var element = data.element;
                        element.$.value = element.$.defaultValue = this.getValue();
                    }
                }
            ]
        };

        var options = {
            title: "多行文本属性",
            minWidth: 400,
            minHeight: 210,
            height: 220,
            width: 400,
            onShow: onShow,
            onOk: onOk,
            onLoad: onLoad,
            contents: [controlOptions]
        };

        return options;
    });

})();