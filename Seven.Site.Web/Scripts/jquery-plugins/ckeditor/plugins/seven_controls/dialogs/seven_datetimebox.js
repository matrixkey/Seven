﻿(function () {

    var dialogName = "seven_datetimebox";

    CKEDITOR.dialog.add(dialogName, function (editor) {

        //常规属性的赋值，如value、size、maxLength、title
        var autoSetup1 = function (element) {
            var value = element.getAttribute(this.id) || "";
            this.setValue(value);
        };

        //单位是px的样式属性的赋值
        var autoSetup2 = function (element) {
            var value = CKEDITOR.tools.convertToPx(element.getStyle(this.id)) || "";
            this.setValue(value);
        };

        //单位不是px的样式属性的赋值
        var autoSetup3 = function (element) {
            var value = element.getStyle(this.id) || "";
            this.setValue(value);
        };

        //常规属性的提交，如value、size、maxLength、title
        var autoCommit1 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setAttribute(this.id, value); }
            else
            { element.removeAttribute(this.id); }
        };

        //单位是px的样式属性的提交
        var autoCommit2 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setStyle(this.id, CKEDITOR.tools.cssLength(value)); }
            else
            { element.removeStyle(this.id); }
        };

        //单位不是px的样式属性的提交
        var autoCommit3 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setStyle(this.id, value); }
            else
            { element.removeStyle(this.id); }
        };

        var htmlAttributes = ["value", "title", "class"];
        var pxCssAttributes = ["width", "font-size"];
        var nopxCssAttributes = ["text-align"];

        var onShow = function () {
            delete this.datetimeBox;

            var element = editor.getSelection().getSelectedElement();
            if (element && element.getName() == "input") {
                var elementType = element.getAttribute("type");
                if (!elementType || elementType == "text") {
                    this.datetimeBox = element;
                    this.setupContent(element);
                }
            }
        };

        var onOk = function () {
            var element = this.datetimeBox, isInsertMode = !element;
            if (isInsertMode) {
                element = editor.document.createElement("input");
                element.setAttribute("type", "text");
                element.addClass('Wdate');
                editor.insertElement(element);
            }
            this.commitContent({ element: element });//此处的参数传入方式，决定了element的commit属性值的参数签名
        };

        var onLoad = function () {
            this.foreach(function (contentObj) {
                if (htmlAttributes.contains(contentObj.id)) {
                    contentObj.setup = autoSetup1;
                    contentObj.commit = autoCommit1;
                }
                else if (pxCssAttributes.contains(contentObj.id)) {
                    contentObj.validate = CKEDITOR.dialog.validate.integer("请输入数字格式。");
                    contentObj.setup = autoSetup2;
                    contentObj.commit = autoCommit2;
                }
                else if (nopxCssAttributes.contains(contentObj.id)) {
                    contentObj.setup = autoSetup3;
                    contentObj.commit = autoCommit3;
                }
            });
        };

        var controlOptions = {
            id: "info",
            label: "单行文本属性",
            title: "单行文本属性",
            elements: [
                {
                    type: "hbox", widths: ["50%", "50%"],
                    children: [
                        { id: "title", type: "text", label: "控件名称", "default": "" },
                        { id: "width", type: "text", label: "控件宽度", "default": "" }
                    ]
                },
                {
                    id: "text-align", type: "select", label: "对齐方式", "default": "",
                    items: [["", ""], ["左对齐", "left"], ["居中", "center"]]
                },
                {
                    id: "showTime", type: "checkbox", label: "显示时间", "default": "", value: "checked",
                    setup: function (element) {
                        this.setValue(element.getAttribute("showTime") || "");
                    },
                    commit: function (data) {
                        var element = data.element,
                            originalChecked = element.getAttribute("showTime"),
                            currentChecked = this.getValue();
                        //此处只能以html属性的方式绑定click事件，编辑器内容要存入数据库。
                        if ((originalChecked != "checked") && currentChecked) {
                            element.setAttribute("onclick", "try{WdatePicker({dateFmt:\"yyyy-MM-dd HH:mm\"});}catch(e){}");
                        }
                        else {
                            element.setAttribute("onclick", 'try{WdatePicker();}catch(e){}');
                        }
                        element.setAttribute("showTime", (currentChecked ? "checked" : ""));
                    }
                }
            ]
        };

        var options = {
            title: "日期控件属性",
            minWidth: 400,
            minHeight: 210,
            height: 220,
            width: 400,
            onShow: onShow,
            onOk: onOk,
            onLoad: onLoad,
            contents: [controlOptions]
        };

        return options;
    });

})();