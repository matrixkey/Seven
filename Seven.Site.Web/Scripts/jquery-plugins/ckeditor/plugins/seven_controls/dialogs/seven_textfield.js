﻿(function () {

    var dialogName = "seven_textfield";

    CKEDITOR.dialog.add(dialogName, function (editor) {

        //常规属性的赋值，如value、size、maxLength、title
        var autoSetup1 = function (element) {
            var value = element.getAttribute(this.id) || "";
            this.setValue(value);
        };

        //单位是px的样式属性的赋值
        var autoSetup2 = function (element) {
            var value = CKEDITOR.tools.convertToPx(element.getStyle(this.id)) || "";
            this.setValue(value);
        };

        //单位不是px的样式属性的赋值
        var autoSetup3 = function (element) {
            var value = element.getStyle(this.id) || "";
            this.setValue(value);
        };

        //常规属性的提交，如value、size、maxLength、title
        var autoCommit1 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setAttribute(this.id, value); }
            else
            { element.removeAttribute(this.id); }
        };

        //单位是px的样式属性的提交
        var autoCommit2 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setStyle(this.id, CKEDITOR.tools.cssLength(value)); }
            else
            { element.removeStyle(this.id); }
        };

        //单位不是px的样式属性的提交
        var autoCommit3 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setStyle(this.id, value); }
            else
            { element.removeStyle(this.id); }
        };

        var htmlAttributes = ["value", "maxLength", "title", "class"];
        var pxCssAttributes = ["width", "font-size"];
        var nopxCssAttributes = ["text-align"];

        var acceptedTypes = ["text", "password"];

        var onShow = function () {
            delete this.textField;

            var element = editor.getSelection().getSelectedElement();
            if (element && element.getName() == "input") {
                var elementType = element.getAttribute("type");
                if (!elementType || acceptedTypes.contains(elementType)) {
                    this.textField = element;
                    this.setupContent(element);
                }
            }
        };

        var onOk = function () {
            var element = this.textField, isInsertMode = !element;
            if (isInsertMode) {
                element = editor.document.createElement("input");
                element.setAttribute("type", "text");
                editor.insertElement(element);
            }
            this.commitContent({ element: element });//此处的参数传入方式，决定了element的commit属性值的参数签名
        };

        var onLoad = function () {
            this.foreach(function (contentObj) {
                if (htmlAttributes.contains(contentObj.id)) {
                    contentObj.setup = autoSetup1;
                    contentObj.commit = autoCommit1;
                }
                else if (pxCssAttributes.contains(contentObj.id)) {
                    contentObj.validate = CKEDITOR.dialog.validate.integer("请输入数字格式。");
                    contentObj.setup = autoSetup2;
                    contentObj.commit = autoCommit2;
                }
                else if (nopxCssAttributes.contains(contentObj.id)) {
                    contentObj.setup = autoSetup3;
                    contentObj.commit = autoCommit3;
                }
            });
        };

        var controlOptions = {
            id: "info",
            label: "单行文本属性",
            title: "单行文本属性",
            elements: [
                {
                    type: "hbox",
                    widths: ["50%", "50%"],
                    children: [
                        { id: "title", type: "text", label: "控件名称", "default": "" },
                        { id: "width", type: "text", label: "控件宽度", "default": "" }
                    ]
                },
                {
                    type: "hbox",
                    widths: ["50%", "50%"],
                    children: [
                        { id: "value", type: "text", label: "初始值", "default": "" }
                    ]
                },
                {
                    type: "hbox",
                    widths: ["50%", "50%"],
                    children: [
                        { id: "font-size", type: "text", label: "字体大小", "default": "12" },
                        {
                            type: "hbox",
                            widths: ["30%", "70%"],
                            children: [
                                {
                                    id: "maxLength", type: "text", label: "最高字符数", "default": "", style: "width:50px",
                                    validate: CKEDITOR.dialog.validate.integer("请输入数字格式。")
                                },
                                { type: "html", html: "<br /><span style=\"color:red;\">空表示不限制字数</span>" }
                            ]
                        }
                    ]
                },
                {
                    type: "hbox",
                    widths: ["50%", "50%"],
                    children: [
                        {
                            id: "class", type: "select", label: "数据类型", "default": "",
                            items: [["", ""], ["网址", "url"], ["整数", "digits"], ["数字", "number"], ["日期型", "dateISO"], ["电子邮件", "email"]]
                        },
                        {
                            id: "text-align", type: "select", label: "对齐方式", "default": "",
                            items: [["", ""], ["左对齐", "left"], ["居中", "center"], ["右对齐", "right"]]
                        }
                    ]
                }
            ]
        };


        var options = {
            title: "单行文本属性",
            minWidth: 400,
            minHeight: 210,
            height: 220,
            width: 400,
            onShow: onShow,
            onOk: onOk,
            onLoad: onLoad,
            contents: [controlOptions]
        };

        return options;
    });



})();