﻿(function () {

    var dialogName = "seven_checkbox";

    CKEDITOR.dialog.add(dialogName, function (editor) {

        //常规属性的赋值，如value、size、maxLength、title
        var autoSetup1 = function (element) {
            var value = element.getAttribute(this.id) || "";
            this.setValue(value);
        };

        //常规属性的提交，如value、size、maxLength、title
        var autoCommit1 = function (data) {
            var element = data.element, value = this.getValue();
            if (value)
            { element.setAttribute(this.id, value); }
            else
            { element.removeAttribute(this.id); }
        };

        var htmlAttributes = ["title", "group"]; //checkbox的value在ie下值是on off，所以要特别处理

        var onShow = function () {
            delete this.checkBox;

            var element = editor.getSelection().getSelectedElement();
            if (element && element.getName() == "input") {
                var elementType = element.getAttribute("type");
                if (elementType == "checkbox") {
                    this.checkBox = element;
                    this.setupContent(element);
                }
            }
        };

        var onOk = function () {
            var element = this.checkBox, isInsertMode = !element;
            if (isInsertMode) {
                element = editor.document.createElement("input");
                element.setAttribute("type", "checkbox");
                editor.insertElement(element);
            }
            this.commitContent({ element: element });//此处的参数传入方式，决定了element的commit属性值的参数签名
        };

        var onLoad = function () {
            this.foreach(function (contentObj) {
                if (htmlAttributes.contains(contentObj.id)) {
                    contentObj.setup = autoSetup1;
                    contentObj.commit = autoCommit1;
                }
            });
        };

        var controlOptions = {
            id: "info",
            label: "复选框属性",
            title: "复选框属性",
            startupFocus: "txtName",
            elements: [
                { id: "title", type: "text", label: "控件名称", "default": "" },
                {
                    id: "Value", type: "text", label: "选定值", "default": "",
                    setup: function (element) {
                        var value = element.getAttribute("value");
                        // IE会返回 'on' 作为默认的 value 属性，要处理掉
                        this.setValue(CKEDITOR.env.ie && value == "on" ? "" : value);
                    },
                    commit: function (data) {
                        var element = data.element, value = this.getValue();
                        if (value) {
                            element.setAttribute("value", value);
                        }
                        else {
                            //value为空值时，执行到这里。在IE下用removeAttribute("value")是无用的。所以要创建新的checkbox去替换。
                            if (CKEDITOR.env.ie) {
                                var checkbox = new CKEDITOR.dom.element("input", element.getDocument());
                                element.copyAttributes(checkbox, { value: 1 }); //value表示copy过程中要忽略的属性
                                checkbox.replace(element); //表示用checkbox替换element，源和目标刚好与js的replace刚好相反
                                editor.getSelection().selectElement(checkbox);
                                data.element = checkbox;
                            }
                            else { element.removeAttribute("value"); }
                        }
                    }
                },
                { id: "group", type: "text", label: "组名", "default": "" },
                {
                    id: "checked", type: "checkbox", label: "已勾选", "default": "", value: "checked",
                    setup: function (element) { this.setValue(element.getAttribute("checked") || ""); },
                    commit: function (data) {
                        var element = data.element,
                            originalChecked = element.getAttribute("checked"),
                            currentChecked = this.getValue();

                        if ((originalChecked == "checked") != currentChecked) {
                            var checkbox = CKEDITOR.dom.element.createFromHtml("<input type=\"checkbox\" " + (currentChecked ? "checked=\"checked\"" : "") + " />");
                            element.copyAttributes(checkbox, { checked: 1 }); //value表示copy过程中要忽略的属性
                            checkbox.replace(element); //表示用checkbox替换element，源和目标刚好与js的replace刚好相反
                            editor.getSelection().selectElement(checkbox);
                            data.element = checkbox;
                        }
                    }
                }
            ]
        };

        var options = {
            title: "复选框属性",
            minWidth: 400,
            minHeight: 210,
            height: 190,
            width: 400,
            onShow: onShow,
            onOk: onOk,
            onLoad: onLoad,
            contents: [controlOptions]
        };

        return options;
    });

})();