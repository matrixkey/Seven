﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.RepositoryModels.WorkflowAbout;
using Seven.DataModel.ViewModels.WorkflowAbout;
using Seven.Tools.Extension;
using Seven.Web.IntelligentQuery.Model;
using Seven.Web.IntelligentQuery.Extensions;

namespace Seven.MsSql.Repositories
{
    public partial class WfFormRepository
    {
        /// <summary>
        /// 根据表单ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">表单ID</param>
        /// <returns></returns>
        public FormModel GetEditModelByID(int id)
        {
            var form = this.Entities().Where(w => w.ID == id);
            var formType = new WfFormTypeRepository(this.dbContext).Entities();

            var query = from a in form
                        join b in formType on a.Type equals b.ID
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.Type,
                            TypeName = b.Name,
                            a.SortNumber
                        };

            var data = query.FirstOrDefault();

            return data.CopyTo<FormModel>();
        }

        /// <summary>
        /// 根据表单ID获取供设计的数据模型
        /// </summary>
        /// <param name="id">表单ID</param>
        /// <returns></returns>
        public FormDesignModel GetDesignModelByID(int id)
        {
            var data = this.Entities().Where(w => w.ID == id).Select(s => new { s.ID, s.Name, s.Content }).FirstOrDefault();

            return data.CopyTo<FormDesignModel>();
        }

        /// <summary>
        /// 根据表单类型ID获取表单数据模型集合
        /// </summary>
        /// <param name="typeId">表单类型ID</param>
        /// <param name="model">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <param name="total">数据总数，输出参数</param>
        /// <returns></returns>
        public IEnumerable<FormGridModel> FormGrid(int typeId, string model, int page, int rows, out int total)
        {
            var form = this.Entities().Where(w => w.Type == typeId);
            var user = new SysUserRepository(this.dbContext).Entities();
            var staff = new HrStaffRepository(this.dbContext).Entities();

            var queryModel = this.ConvertQueryModel(model);
            if (queryModel != null) { form = form.Where(queryModel); }

            var query = from a in form
                        join b in user on a.CreateUserName equals b.UserName into ab
                        from UserInfo in ab.DefaultIfEmpty()
                        join c in staff on UserInfo.StaffID equals c.ID into ac
                        from CreaterInfo in ac.DefaultIfEmpty()
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.SortNumber,
                            a.Remark,
                            CreateUserRealName = CreaterInfo == null ? "" : CreaterInfo.RealName
                        };
            total = query.Count();
            var data = query.OrderBy(o => o.ID).SplitPage(page - 1, rows).ToArray();

            return data.Select(s => s.CastTo<FormGridModel>(false));
        }
    }
}
