﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Seven.Entity;
using Seven.DataModel.ViewModels.MenuAbout;
using Seven.Tools.Extension;

namespace Seven.MsSql.Repositories
{
    public partial class SysMenuFunctionRepository
    {
        /// <summary>
        /// 根据菜单功能点ID获取供编辑的数据模型
        /// </summary>
        /// <param name="id">菜单功能点ID</param>
        /// <returns></returns>
        public MenuFunctionModel GetEditModelByID(int id)
        {
            var data = this.Entities().Where(w => w.ID == id)
                .Select(s => new
                {
                    s.ID,
                    s.Name,
                    s.Type,
                    s.SortNumber,
                    s.MenuID,
                    MenuName = s.Menu.Name,
                    s.Menu.ControllerName,
                    s.ActionName,
                    s.ActionDescription,
                    s.IsShow,
                    s.Remark
                })
                .FirstOrDefault();

            return data.CastTo<MenuFunctionModel>();
        }

        /// <summary>
        /// 根据菜单ID获取菜单功能点集合
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        public IEnumerable<SysMenuFunction> GetMenuFunctionsByMenuID(int menuId)
        {
            return this.Entities().Where(w => w.MenuID == menuId).OrderBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 根据菜单功能点ID集合获取菜单功能点集合
        /// </summary>
        /// <param name="ids">菜单功能点ID集合</param>
        /// <param name="includeMenu">是否同时查询导航属性-菜单的数据</param>
        /// <returns></returns>
        public IEnumerable<SysMenuFunction> GetMenuFunctionsByIDs(IEnumerable<int> ids, bool includeMenu = false)
        {
            if (includeMenu) { return this.Entities().Where(w => ids.Contains(w.ID)).Include(i => i.Menu).OrderBy(o => o.SortNumber).ToArray(); }
            else
            { return this.Entities().Where(w => ids.Contains(w.ID)).OrderBy(o => o.SortNumber).ToArray(); }
        }

        /// <summary>
        /// 获取所有菜单功能点集合
        /// </summary>
        /// <param name="includeMenu">是否同时查询导航属性-菜单的数据</param>
        /// <returns></returns>
        public IEnumerable<SysMenuFunction> GetAllMenuFunctions(bool includeMenu = false)
        {
            if (includeMenu)
            { return this.Entities().Include(i => i.Menu).OrderBy(o => o.MenuID).ThenBy(o => o.SortNumber).ToArray(); }
            else
            { return this.Entities().OrderBy(o => o.MenuID).ThenBy(o => o.SortNumber).ToArray(); }
        }
    }
}
