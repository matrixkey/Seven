﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;

using Seven.EntityBasic;
using Seven.Core.Helper;

namespace Seven.MsSql.Configuration
{
    /// <summary>
    /// 实体配置基类
    /// </summary>
    /// <typeparam name="TEntity">实体类型</typeparam>
    /// <typeparam name="TKey">实体类型的主键类型</typeparam>
    public abstract class EntityConfigurationBase<TEntity, TKey> : EntityTypeConfiguration<TEntity>, IEntityConfiguration
        where TKey : struct
        where TEntity : EntityBase<TKey>
    {
        public EntityConfigurationBase()
        {
            string TableName = EntityAction.GetTableName<TEntity, TKey>();
            if (!string.IsNullOrWhiteSpace(TableName))
            { ToTable(TableName); }
        }

        public void RegistTo(ConfigurationRegistrar configurations)
        {
            configurations.Add(this);
        }
    }
}
