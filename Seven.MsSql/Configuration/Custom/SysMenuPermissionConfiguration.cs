﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.MsSql.Configuration
{
    partial class SysMenuPermissionConfiguration
    {
        partial void SysMenuPermissionConfigurationAppend()
        {
            HasRequired(m => m.Menu).WithMany().HasForeignKey(f => f.MenuID);
        }
    }
}
