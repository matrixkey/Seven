﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;

using Seven.MsSql.Configuration;

namespace Seven.MsSql.Context
{
    public class EFDbContext : DbContext
    {
        public EFDbContext()
            : base(DbConfig.ConnString)
        {
            // 禁用验证所跟踪实体
            //Configuration.ValidateOnSaveEnabled = false;
            // 禁用自动检测功能
            //Configuration.AutoDetectChangesEnabled = false;
        }

        public EFDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            // 禁用验证所跟踪实体
            //Configuration.ValidateOnSaveEnabled = false;
            // 禁用自动检测功能
            //Configuration.AutoDetectChangesEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // 禁用一对多级联删除
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            // 禁用多对多级联删除
            //modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            foreach (var configuration in EntityConfigurationFactory.Configurations)
            {
                configuration.RegistTo(modelBuilder.Configurations);
            }

            //modelBuilder.Configurations.Add(new HrOrganizationConfiguration());
            //modelBuilder.Configurations.Add(new HrStaffConfiguration());
            //modelBuilder.Configurations.Add(new SysMenuConfiguration());
            //modelBuilder.Configurations.Add(new SysMenuFunctionConfiguration());
            //modelBuilder.Configurations.Add(new SysUserConfiguration());
            //modelBuilder.Configurations.Add(new SysUserBaseConfigConfiguration());
        }
    }
}
