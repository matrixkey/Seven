﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Member
{
    public class UserInfoModel
    {
        /// <summary>
        /// 身份令牌标识
        /// </summary>
        public Guid Token { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户别名
        /// </summary>
        public string LoginCode { get; set; }

        /// <summary>
        /// 登录时所用的帐号（用户名或者登录别名）
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleID { get; set; }

        /// <summary>
        /// 所属集团ID
        /// </summary>
        public int GroupID { get; set; }

        /// <summary>
        /// 所属公司ID
        /// </summary>
        public int CompanyID { get; set; }

        /// <summary>
        /// 所属部门ID
        /// </summary>
        public int DeptID { get; set; }
    }
}
