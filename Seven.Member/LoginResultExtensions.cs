﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Seven.Tools.Extension;

namespace Seven.Member
{
    /// <summary>
    /// 用于快速处理 登录状态、登出状态已经用户在线状态 枚举值的解析。
    /// </summary>
    public static class LoginResultExtensions
    {
        #region LoginResult

        /// <summary>
        /// 获取声明 <see cref="LoginResult"/> 枚举值字段时所为其定义的 <see cref="DescriptionAttribute"/> 特性值。
        /// </summary>
        /// <param name="loginResult"></param>
        /// <returns></returns>
        public static DescriptionAttribute GetDescription(this LoginResult loginResult)
        {
            return loginResult.GetCustomeAttributes<DescriptionAttribute>().FirstOrDefault();
        }

        /// <summary>
        /// 获取声明 <see cref="LoginResult"/> 枚举值字段时所为其定义的 <see cref="DescriptionAttribute"/> 特性的 Description 属性值。
        /// 该属性值表示 <see cref="LoginResult"/> 枚举值所包含的具体信息说明。
        /// </summary>
        /// <param name="loginResult"></param>
        /// <returns></returns>
        public static string GetDescriptionValue(this LoginResult loginResult)
        {
            DescriptionAttribute descAttr = GetDescription(loginResult);
            return descAttr != null ? descAttr.Description : loginResult.ToString();
        }

        #endregion

        #region LogoutResult

        /// <summary>
        /// 获取声明 <see cref="LogoutResult"/> 枚举值字段时所为其定义的 <see cref="DescriptionAttribute"/> 特性值。
        /// </summary>
        /// <param name="logoutResult"></param>
        /// <returns></returns>
        public static DescriptionAttribute GetDescription(this LogoutResult logoutResult)
        {
            return logoutResult.GetCustomeAttributes<DescriptionAttribute>().FirstOrDefault();
        }

        /// <summary>
        /// 获取声明 <see cref="LogoutResult"/> 枚举值字段时所为其定义的 <see cref="DescriptionAttribute"/> 特性的 Description 属性值。
        /// 该属性值表示 <see cref="LogoutResult"/> 枚举值所包含的具体信息说明。
        /// </summary>
        /// <param name="logoutResult"></param>
        /// <returns></returns>
        public static string GetDescriptionValue(this LogoutResult logoutResult)
        {
            DescriptionAttribute descAttr = GetDescription(logoutResult);
            return descAttr != null ? descAttr.Description : logoutResult.ToString();
        }

        #endregion

        #region LoginState

        /// <summary>
        /// 获取声明 <see cref="LoginState"/> 枚举值字段时所为其定义的 <see cref="DescriptionAttribute"/> 特性值。
        /// </summary>
        /// <param name="loginState"></param>
        /// <returns></returns>
        public static DescriptionAttribute GetDescription(this LoginState loginState)
        {
            return loginState.GetCustomeAttributes<DescriptionAttribute>().FirstOrDefault();
        }

        /// <summary>
        /// 获取声明 <see cref="LoginState"/> 枚举值字段时所为其定义的 <see cref="DescriptionAttribute"/> 特性的 Description 属性值。
        /// 该属性值表示 <see cref="LogoutResult"/> 枚举值所包含的具体信息说明。
        /// </summary>
        /// <param name="loginState"></param>
        /// <returns></returns>
        public static string GetDescriptionValue(this LoginState loginState)
        {
            DescriptionAttribute descAttr = GetDescription(loginState);
            return descAttr != null ? descAttr.Description : loginState.ToString();
        }

        #endregion
    }
}
