﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Tools.Extension
{
    /// <summary>
    /// 提供一组对字典对象 <see cref="System.Collections.Generic.Dictionary&lt;TKey, TValue&gt;"/> 操作方法的扩展。
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// 返回IDictionary对象的第一个元素的Value值，若对象为空或元素个数为0，则返回Value类型的默认值
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="_this"></param>
        /// <returns></returns>
        public static TValue FirstValueOrDefault<TKey,TValue>(this IDictionary<TKey, TValue> _this)
        {
            if (_this == null || _this.Count == 0) { return default(TValue); }

            return _this.First().Value;
        }

        /// <summary>
        /// 根据键的值获取与该键关联的值。
        /// <para>该扩展方法与 <see cref="System.Collections.Generic.Dictionary&lt;TKey, TValue&gt;"/> 字典对象的默认索引器 get 方法差别在于：</para>
        /// <para>    默认索引器 get 方法在找不到指定的键时，会引发 <see cref="System.Collections.Generic.KeyNotFoundException"/> 异常；</para>
        /// <para>    而该方法在找不到指定的键时候，返回值将为 <typeparamref name="TValue"/> 类型的默认值。</para>
        /// <para>如果需要在调用该方法后确认是否成功获取到 <paramref name="key"/> 键对应的值，请使用带有 out bool success 参数的 GetValue 方法。</para>
        /// </summary>
        /// <typeparam name="TKey">字典中的键的类型。</typeparam>
        /// <typeparam name="TValue">字典中的值的类型。</typeparam>
        /// <param name="_this">表示当前 <see cref="System.Collections.Generic.Dictionary&lt;TKey, TValue&gt;"/> 字典对象。</param>
        /// <param name="key">字典中的键。</param>
        /// <returns>返回一个 <typeparamref name="TValue"/> 类型值。</returns>
        public static TValue GetValue<TKey, TValue>(this IDictionary<TKey, TValue> _this, TKey key)
        {
            Check.NotNull(_this);
            TValue ret;
            _this.TryGetValue(key, out ret);
            return ret;
        }


    }
}
