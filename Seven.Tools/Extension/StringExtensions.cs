﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.Tools.Extension
{
    /// <summary>
    /// 提供一组对字符串对象 <see cref="System.String"/> 操作方法的扩展。
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// 将字符串转换为其所表示的 int 值。
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ToInt(this string value)
        {
            return ToInt(value, 0);
        }

        /// <summary>
        /// 将字符串转换为其所表示的 int 值。参数表示转换不成功时返回的默认值。
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int ToInt(this string value, int defaultValue)
        {
            int ret;
            return int.TryParse(value, out ret) ? ret : defaultValue;
        }

        /// <summary>
        /// 将字符串转换为其所表示的 bool 值。
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ToBoolean(this string value)
        {
            return ToBoolean(value, default(bool));
        }

        /// <summary>
        /// 将字符串转换为其表示的 bool 值。参数表示转换不成功时返回的默认值。
        /// </summary>
        /// <param name="value"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static bool ToBoolean(this string value, bool defaultValue)
        {
            bool ret;
            return Boolean.TryParse(value, out ret) ? ret : defaultValue;
        }


        #region 对string[]的扩展

        /// <summary>
        /// 将字符串数组转换为其所表示的 int 值集合。
        /// </summary>
        /// <param name="value"></param>
        /// <param name="distinct">是否去掉重复值，默认是。</param>
        /// <returns></returns>
        public static IEnumerable<int> ToInt(this string[] value, bool distinct = true)
        {
            List<int> result = new List<int>();
            foreach (var item in value)
            {
                result.Add(item.ToInt());
            }
            return distinct ? result.ToArray().Distinct() : result.ToArray();
        }

        #endregion
    }
}
