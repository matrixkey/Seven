﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.NoSql.Redis
{
    internal class Config
    {
        /// <summary>
        /// Redis服务器的IP
        /// </summary>
        public static string RedisServerIP = "127.0.0.1";

        /// <summary>
        /// Redis服务器的端口
        /// </summary>
        public static int RedisServerPort = 6379;
    }
}
