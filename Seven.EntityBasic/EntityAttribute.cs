﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.EntityBasic
{
    /// <summary>
    /// 实体专用特性，需要自定义实体对应的数据库表名时，使用本特性
    /// </summary>
    public class EntityAttribute : Attribute
    {
        /// <summary>
        /// 实际名称
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="tableName">实际名称</param>
        public EntityAttribute(string tableName)
        {
            TableName = tableName;
        }
    }
}
