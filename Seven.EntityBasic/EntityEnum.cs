﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seven.EntityBasic
{
    /// <summary>
    /// 机构类型
    /// </summary>
    public enum OrganType
    {
        大集团 = 1,
        子集团 = 2,
        公司 = 4,
        部门 = 8
    }

    /// <summary>
    /// 性别
    /// </summary>
    public enum Sex
    { 
        男 = 1,
        女 = 2,
        保密 = 4
    }

    /// <summary>
    /// 功能点类型
    /// </summary>
    public enum FunctionType
    { 
        页面 = 1,
        提供数据 = 2,
        操作 = 4
    }

    /// <summary>
    /// 权限类型
    /// </summary>
    public enum PermissionType
    {
        访问 = 1,
        按钮 = 2,
        数据 = 4
    }

    #region 桌面模块

    /// <summary>
    /// 位置
    /// </summary>
    public enum DeskModuleLocation
    {
        左边 = 1,
        右边 = 2
    }

    /// <summary>
    /// 控制类型
    /// </summary>
    public enum DeskModuleControlType
    {
        用户必选 = 1,
        用户可选 = 2
    }

    #endregion
}
